package me.zombiegalaxydev.faction.kits;
import me.zombiegalaxydev.faction.utils.FactionItems;
import me.zombieghostdev.gameapi.client.Rank;
import me.zombieghostdev.gameapi.utils.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class BurnoutKit implements FactionKit {

  public String name () {
    return "§e§lBurnout Kit";
  }

  @Override
  public Rank rank () {
    return Rank.IRON;
  }

  public ItemStack icon () {
    ItemStack item = new ItemStack (Material.TORCH);
    ItemMeta meta = item.getItemMeta ();
    meta.setDisplayName (name ());
    item.setItemMeta (meta);
    return item;
  }

  public ItemStack[] contents () {

    return new ItemStack[]{
	   new ItemStackBuilder (Material.IRON_HELMET).getDisplayName ("§e§lBurnout Helmet").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).build (),
	   new ItemStackBuilder (Material.IRON_CHESTPLATE).getDisplayName ("§e§lBurnout Chestplate").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).build (),
	   new ItemStackBuilder (Material.IRON_LEGGINGS).getDisplayName ("§e§lBurnout Legging").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).build (),
	   new ItemStackBuilder (Material.IRON_BOOTS).getDisplayName ("§e§lBurnout Boots").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).build (),
	   new ItemStackBuilder (Material.DIAMOND_SWORD).getDisplayName ("§e§lBurnout Sword").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.DAMAGE_ALL, 3).build (),
	   new ItemStackBuilder (Material.DIAMOND_SWORD).getDisplayName ("§e§lBurnout Sword").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.DAMAGE_ALL, 3).build (),
	   new ItemStackBuilder (Material.DIAMOND_PICKAXE).getDisplayName ("§e§lBurnout Pickaxe").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.DIG_SPEED, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_PICKAXE).getDisplayName ("§e§lBurnout Pickaxe").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.DIG_SPEED, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_SPADE).getDisplayName ("§e§lBurnout Shovel").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.DIG_SPEED, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_AXE).getDisplayName ("§e§lBurnout Axe").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.DIG_SPEED, 2).build (),
	   new ItemStackBuilder (Material.BOW).getDisplayName ("§e§lBurnout Bow").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.ARROW_DAMAGE, 2).build (),
	   new ItemStack (Material.GOLDEN_APPLE, 4), new ItemStack (Material.COOKED_BEEF, 32), new ItemStack (Material.OBSIDIAN, 8), new ItemStack (Material.TNT, 32), new ItemStack (Material.ARROW, 64)
	   , FactionItems.getMoneyNote (25000)
    };
  }
  @Override
  public long coolDown () {
    return 20*(60*60);
  }

}
