package me.zombiegalaxydev.faction.kits;
import me.zombiegalaxydev.faction.utils.FactionItems;
import me.zombieghostdev.gameapi.client.Rank;
import me.zombieghostdev.gameapi.utils.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class StarterKit implements FactionKit {


  public String name () {
    return "§e§lStarter Kit";
  }

  @Override
  public Rank rank () {
    return Rank.NORMAL;
  }

  public ItemStack icon () {
    return new ItemStackBuilder (Material.getMaterial (58)).getDisplayName (name ()).build ();
  }

  public ItemStack[] contents () {
    return new ItemStack[]{
	   new ItemStack (Material.IRON_HELMET),
	   new ItemStack (Material.IRON_CHESTPLATE),
	   new ItemStack (Material.IRON_LEGGINGS),
	   new ItemStack (Material.IRON_BOOTS),
	   new ItemStack (Material.IRON_SWORD),
	   new ItemStack (Material.BOW),
	   new ItemStack (Material.ARROW, 32),
	   new ItemStack (Material.IRON_PICKAXE),
	   new ItemStack (Material.IRON_AXE),
	   new ItemStack (Material.IRON_SPADE),
	   new ItemStack (Material.COOKED_BEEF, 64),
	   FactionItems.getMoneyNote (500)
    };
  }

  public long coolDown () {
    return 3*(60*60);
  }
}
