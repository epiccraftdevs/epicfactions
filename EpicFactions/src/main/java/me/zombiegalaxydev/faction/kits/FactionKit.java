package me.zombiegalaxydev.faction.kits;
import me.zombieghostdev.gameapi.client.Rank;
import org.bukkit.inventory.ItemStack;

public interface FactionKit {
  String name ();
  Rank rank ();
  ItemStack icon ();
  ItemStack[] contents ();
  long coolDown ();
}
