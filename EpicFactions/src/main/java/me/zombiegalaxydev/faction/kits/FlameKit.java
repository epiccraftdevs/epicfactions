package me.zombiegalaxydev.faction.kits;
import me.zombiegalaxydev.faction.utils.FactionItems;
import me.zombieghostdev.gameapi.client.Rank;
import me.zombieghostdev.gameapi.utils.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class FlameKit implements FactionKit {

  public String name () {
    return "§c§lFlame Kit";
  }

  @Override
  public Rank rank () {
    return Rank.DIAMOND;
  }

  public ItemStack icon () {
    ItemStack item = new ItemStack (Material.FIREBALL);
    ItemMeta meta = item.getItemMeta ();
    meta.setDisplayName (name ());
    item.setItemMeta (meta);
    return item;
  }

  public ItemStack[] contents () {

    return new ItemStack[]{
	   new ItemStackBuilder (Material.DIAMOND_HELMET).getDisplayName ("§c§lFlame Helmet").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.PROTECTION_PROJECTILE, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getDisplayName ("§c§lFlame Chestplate").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.PROTECTION_PROJECTILE, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getDisplayName ("§c§lFlame Legging").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.PROTECTION_PROJECTILE, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_BOOTS).getDisplayName ("§c§lFlame Boots").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.PROTECTION_PROJECTILE, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_SWORD).getDisplayName ("§c§lFlame Sword").getUnsafeEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.DAMAGE_ALL, 5).getUnsafeEnchantment (Enchantment.KNOCKBACK, 3).getUnsafeEnchantment (Enchantment.FIRE_ASPECT, 1).build (),
	   new ItemStackBuilder (Material.DIAMOND_SWORD).getDisplayName ("§c§lFlame Sword").getUnsafeEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.DAMAGE_ALL, 5).getEnchantment (Enchantment.LOOT_BONUS_MOBS, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_PICKAXE).getDisplayName ("§c§lFlame Pickaxe").getUnsafeEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.DIG_SPEED, 4).getEnchantment (Enchantment.LOOT_BONUS_BLOCKS, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_PICKAXE).getDisplayName ("§c§lFlame Pickaxe").getUnsafeEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.DIG_SPEED, 4).build (),
	   new ItemStackBuilder (Material.DIAMOND_SPADE).getDisplayName ("§c§lFlame Shovel").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.DIG_SPEED, 4).getEnchantment (Enchantment.SILK_TOUCH, 1).build (),
	   new ItemStackBuilder (Material.DIAMOND_SPADE).getDisplayName ("§c§lFlame Shovel").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.DIG_SPEED, 4).build (),
	   new ItemStackBuilder (Material.DIAMOND_AXE).getDisplayName ("§c§lFlame Axe").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.DIG_SPEED, 4).build (),
	   new ItemStackBuilder (Material.BOW).getDisplayName ("§c§lFlame Bow").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.ARROW_DAMAGE, 4).getEnchantment (Enchantment.ARROW_KNOCKBACK, 2).getEnchantment (Enchantment.ARROW_INFINITE, 1).build (),
	   new ItemStack (Material.GOLDEN_APPLE, 5), new ItemStack (Material.GOLDEN_APPLE, 1, (short) 1), new ItemStack (Material.COOKED_BEEF, 32), new ItemStack (Material.OBSIDIAN, 32), new ItemStack (Material.TNT, 64), new ItemStack (Material.ARROW), new ItemStack (Material.BEDROCK, 16), new ItemStack (Material.ENDER_PEARL, 10),
	   FactionItems.getMoneyNote (75000)
    };
  }
  @Override
  public long coolDown () {
    return 30*(60*60);
  }

}
