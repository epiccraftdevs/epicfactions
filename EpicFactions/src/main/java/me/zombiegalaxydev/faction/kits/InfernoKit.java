package me.zombiegalaxydev.faction.kits;
import me.zombiegalaxydev.faction.utils.FactionItems;
import me.zombieghostdev.gameapi.client.Rank;
import me.zombieghostdev.gameapi.utils.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InfernoKit implements FactionKit {

  public String name () {
    return "§4§lInferno Kit";
  }

  @Override
  public Rank rank () {
    return Rank.DIAMOND;
  }

  public ItemStack icon () {
    ItemStack item = new ItemStack (Material.REDSTONE_BLOCK);
    ItemMeta meta = item.getItemMeta ();
    meta.setDisplayName (name ());
    item.setItemMeta (meta);
    return item;
  }

  public ItemStack[] contents () {

    return new ItemStack[]{
	   new ItemStackBuilder (Material.DIAMOND_HELMET).getDisplayName ("§4§lInferno Helmet").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.PROTECTION_PROJECTILE, 4).getEnchantment (Enchantment.OXYGEN, 3).build (),
	   new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getDisplayName ("§4§lInferno Chestplate").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.PROTECTION_PROJECTILE, 4).getEnchantment (Enchantment.PROTECTION_FIRE, 4).build (),
	   new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getDisplayName ("§4§lInferno Legging").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.PROTECTION_PROJECTILE, 4).getEnchantment (Enchantment.PROTECTION_FIRE, 4).build (),
	   new ItemStackBuilder (Material.DIAMOND_BOOTS).getDisplayName ("§4§lInferno Boots").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.PROTECTION_PROJECTILE, 4).getEnchantment (Enchantment.PROTECTION_FALL, 4).build (),
	   new ItemStackBuilder (Material.DIAMOND_SWORD).getDisplayName ("§4§lInferno Sword").getUnsafeEnchantment (Enchantment.DURABILITY, 4).getEnchantment (Enchantment.DAMAGE_ALL, 5).getUnsafeEnchantment (Enchantment.KNOCKBACK, 3).getUnsafeEnchantment (Enchantment.FIRE_ASPECT, 3).build (),
	   new ItemStackBuilder (Material.DIAMOND_SWORD).getDisplayName ("§4§lInferno Sword").getUnsafeEnchantment (Enchantment.DURABILITY, 4).getEnchantment (Enchantment.DAMAGE_ALL, 5).getEnchantment (Enchantment.LOOT_BONUS_MOBS, 3).build (),
	   new ItemStackBuilder (Material.DIAMOND_PICKAXE).getDisplayName ("§4§lInferno Pickaxe").getUnsafeEnchantment (Enchantment.DURABILITY, 4).getEnchantment (Enchantment.DIG_SPEED, 3).getEnchantment (Enchantment.LOOT_BONUS_BLOCKS, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_PICKAXE).getDisplayName ("§4§lInferno Pickaxe").getUnsafeEnchantment (Enchantment.DURABILITY, 4).getEnchantment (Enchantment.DIG_SPEED, 3).getEnchantment (Enchantment.SILK_TOUCH, 1).build (),
	   new ItemStackBuilder (Material.DIAMOND_SPADE).getDisplayName ("§4§lInferno Shovel").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.DIG_SPEED, 3).getEnchantment (Enchantment.SILK_TOUCH, 1).build (),
	   new ItemStackBuilder (Material.DIAMOND_SPADE).getDisplayName ("§4§lInferno Shovel").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.DIG_SPEED, 3).build (),
	   new ItemStackBuilder (Material.DIAMOND_AXE).getDisplayName ("§4§lInferno Axe").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.DIG_SPEED, 4).build (),
	   new ItemStackBuilder (Material.BOW).getDisplayName ("§4§lInferno Bow").getEnchantment (Enchantment.DURABILITY, 3).getEnchantment (Enchantment.ARROW_DAMAGE, 5).getEnchantment (Enchantment.ARROW_KNOCKBACK, 2).getEnchantment (Enchantment.ARROW_FIRE, 1).getEnchantment (Enchantment.ARROW_INFINITE, 1).build (),
	   new ItemStack (Material.GOLDEN_APPLE, 10), new ItemStack (Material.GOLDEN_APPLE, 2, (short) 1), new ItemStack (Material.COOKED_BEEF, 64), new ItemStack (Material.OBSIDIAN, 64), new ItemStack (Material.TNT, 128), new ItemStack (Material.ARROW), new ItemStack (Material.BEDROCK, 32), new ItemStack (Material.ENDER_PEARL, 30),
	   new ItemStack (Material.POTION, 6, (short) 8226), new ItemStack (Material.POTION, 6, (short) 8233), FactionItems.getMoneyNote (100000)
    };
  }
  @Override
  public long coolDown () {
    return 35*(60*60);
  }

}
