package me.zombiegalaxydev.faction.kits;
import me.zombiegalaxydev.faction.utils.FactionItems;
import me.zombieghostdev.gameapi.client.Rank;
import me.zombieghostdev.gameapi.utils.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class BlazeKit implements FactionKit {

  public String name () {
    return "§6§lBlaze Kit";
  }

  @Override
  public Rank rank () {
    return Rank.GOLD;
  }
  public ItemStack icon () {
    ItemStack item = new ItemStack (Material.BLAZE_POWDER);
    ItemMeta meta = item.getItemMeta ();
    meta.setDisplayName (name ());
    item.setItemMeta (meta);
    return item;
  }

  public ItemStack[] contents () {

    return new ItemStack[]{
	   new ItemStackBuilder (Material.DIAMOND_HELMET).getDisplayName ("§6§lBlaze Helmet").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).build (),
	   new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getDisplayName ("§6§lBlaze Chestplate").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).build (),
	   new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getDisplayName ("§6§lBlaze Legging").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).build (),
	   new ItemStackBuilder (Material.DIAMOND_BOOTS).getDisplayName ("§6§lBlaze Boots").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).build (),
	   new ItemStackBuilder (Material.DIAMOND_SWORD).getDisplayName ("§6§lBlaze Sword").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.DAMAGE_ALL, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_PICKAXE).getDisplayName ("§6§lBlaze Pickaxe").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.DIG_SPEED, 1).build (),
	   new ItemStackBuilder (Material.DIAMOND_PICKAXE).getDisplayName ("§6§lBlaze Pickaxe").getEnchantment (Enchantment.DURABILITY, 2).build (),
	   new ItemStackBuilder (Material.DIAMOND_SPADE).getDisplayName ("§6§lBlaze Shovel").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.DIG_SPEED, 1).build (),
	   new ItemStackBuilder (Material.DIAMOND_AXE).getDisplayName ("§6§lBlaze Axe").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.DIG_SPEED, 1).build (),
	   new ItemStackBuilder (Material.BOW).getDisplayName ("§6§lBlaze Bow").getEnchantment (Enchantment.DURABILITY, 2).getEnchantment (Enchantment.ARROW_DAMAGE, 1).build (),
	   new ItemStack (Material.GOLDEN_APPLE, 5), new ItemStack (Material.COOKED_BEEF, 32), new ItemStack (Material.OBSIDIAN, 16), new ItemStack (Material.TNT, 32), new ItemStack (Material.ARROW, 64), new ItemStack (Material.BEDROCK, 2)
	   , FactionItems.getMoneyNote (50000)
    };
  }
  @Override
  public long coolDown () {
    return 25*(60*60);
  }

}
