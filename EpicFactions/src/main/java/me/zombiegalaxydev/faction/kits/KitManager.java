package me.zombiegalaxydev.faction.kits;
import me.zombiegalaxydev.faction.Core;
import me.zombiegalaxydev.faction.utils.events.KitUseEvent;
import me.zombieghostdev.gameapi.client.ClientManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class KitManager implements Listener {

  private List<FactionKit> factionKits;

  public KitManager () {
    this.factionKits = new ArrayList<> ();
    factionKits.add (new StarterKit ());
    factionKits.add (new BurnoutKit ());
    factionKits.add (new BlazeKit ());
    factionKits.add (new FlameKit ());
    factionKits.add (new InfernoKit ());
  }

  public FactionKit valueOf (String name){
    for (FactionKit kit : factionKits){
	 if (!kit.name ().equalsIgnoreCase (name))continue;
	 return kit;
    }
    return null;
  }

  public Inventory openKitGUI(Player p){
    Inventory inv = Bukkit.createInventory (null, 9, "§0§lSelect a kit");
    for (FactionKit kit : factionKits){
	 ItemStack item = kit.icon ();
	 ItemMeta meta = item.getItemMeta ();
	 {
	   List<String> lore = new ArrayList<> ();
	   lore.add (" ");
	   lore.add ("§3▶ §b§lContents§8:");
	   for (ItemStack items : kit.contents ()) {
		lore.add ("§8§l-§3 " + items.getType ().name ().replace ('_', ' ') + " X" + items.getAmount ());
	   }
	   lore.add ("  ");
	   lore.add ("§5§l▶ §d§lShift click to preview §5§l◀");
	   if (kit.rank ().ordinal () <= ClientManager.get ().get (p).getRank ().ordinal ()) {
		lore.add ("§6§l▶ §e§lRight Click to receive §6§l◀");
	   } else {
		lore.add ("§4§l▶ §c§lYou don't own this kit §4§l◀");
	   }
	   meta.setLore (lore);
	 }
	 item.setItemMeta (meta);
	 inv.addItem (item);
    }
    p.openInventory (inv);
    return inv;
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onInventory(InventoryClickEvent e){
    Player p = (Player) e.getWhoClicked ();
    ItemStack item = e.getCurrentItem ();
    ClickType click = e.getClick ();

    {
	 for (FactionKit kit : factionKits){
	   if (e.getInventory ().getTitle ().equalsIgnoreCase (kit.name ())){
		e.setCancelled (true);
		return;
	   }
	 }
    }

    if (e.getInventory ().getTitle ().equalsIgnoreCase ("§0§lSelect a kit")) {
	 if (item == null||item.getType ().equals (Material.AIR)) return;
	 FactionKit kit = null;
	 for (FactionKit kits : factionKits) {
	   if (item.getItemMeta ().getDisplayName ().equals (kits.name ())) {
		kit = kits;
	   }
	 }
	 if (kit != null) {
	   e.setCancelled (true);
	   if (click.isRightClick ()) {
		if (kit.rank ().ordinal ()> ClientManager.get ().get (p).getRank ().ordinal ()) return;
		Bukkit.getPluginManager ().callEvent (new KitUseEvent (kit, p));
	   } else if (click.isShiftClick ()) {
		Inventory inv = Bukkit.createInventory (null, 36, kit.name ());
		inv.addItem (kit.contents ());
		p.closeInventory ();
		p.openInventory (inv);
	   }
	 }
    }
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onCloseInventory(final InventoryCloseEvent e){
    for (FactionKit kit : factionKits){
	 if (kit.name ().equalsIgnoreCase (e.getInventory ().getTitle ())){
	   new BukkitRunnable () {
		public void run () {
		 openKitGUI ((Player) e.getPlayer ());
		}
	   }.runTaskLater (Core.get (), 1L);
	 }
    }
  }

  public long getCoolDown(Player player, FactionKit kit){
    Configuration config = Core.get ().getConfig ();
    String query = player.getUniqueId ().toString () + ".kits." + ChatColor.stripColor (kit.name ());
    ConfigurationSection section = config.getConfigurationSection (query);
    if (section==null){
	 section = config.createSection (query);
    }
    return section.getLong ("coolDown");
  }

  public void setCoolDown(Player player, FactionKit kit){
    Configuration config = Core.get ().getConfig ();
    String query = player.getUniqueId ().toString () + ".kits." + ChatColor.stripColor (kit.name ());
    ConfigurationSection section = config.getConfigurationSection (query);
    if (section==null){
	 section = config.createSection (query);
    }
    section.set ("coolDown", System.currentTimeMillis () + (kit.coolDown () * 1000));
    Core.get ().saveConfig ();
  }

  public boolean isCoolDownOver(Player player, FactionKit kit){
    return getCoolDown (player, kit) >= System.currentTimeMillis ();
  }

  public boolean isDefined(Player player, FactionKit kit){
    Configuration config = Core.get ().getConfig ();
    String query = player.getUniqueId ().toString () + ".kits." + ChatColor.stripColor (kit.name ());
    ConfigurationSection section = config.getConfigurationSection (query);
    if (section==null){
	 section = config.createSection (query);
    }
    return section.get ("coolDown")!=null;
  }

  public void giveKit(Player player, FactionKit kit){

    int amount = 0;

    for (ItemStack item : player.getInventory ()){
	 if (item==null||item.getType ().equals (Material.AIR))continue;
	 amount++;
    }

    if (amount+kit.contents ().length>player.getInventory ().getSize ()){
	 player.sendMessage ("§4§l<!>§c You have too much room in you inventory to use this kit.");
	 return;
    }
    player.getInventory ().addItem (kit.contents ());
    player.closeInventory ();
    setCoolDown (player, kit);
    player.sendMessage ("§2§l<!>§a You received the " + kit.name () + " kit!");
    player.sendMessage ("§6§l<?>§e CoolDown for this kit is " + Core.timerTans (kit.coolDown ()) + " seconds!");
  }
}
