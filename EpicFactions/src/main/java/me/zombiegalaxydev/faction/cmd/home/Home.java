package me.zombiegalaxydev.faction.cmd.home;
import org.bukkit.Location;

public class Home {

  private String name; //Name of the home
  private Location location; //Home's location

  /**
   * @author Zombie
   *
   * @param name of the home.
   * @param location of the home.
   */
  public Home (String name, Location location) {
    this.name = name;
    this.location = location;
  }

  /**
   * @author Zombie
   *
   * @return name of the home.
   */
  public String getName () {
    return name;
  }

  /**
   * @author Zombie
   *
   * @return location of the home.
   */
  public Location getLocation () {
    return location;
  }

  /**
   * @author Zombie
   *
   * @return config line for the home.
   */
  public String getConfigLine(){
    return name + "|" +
	   location.getWorld ().getName () + "|" +
	   location.getX () + "|" +
	   location.getY () + "|" +
	   location.getZ () + "|";
  }
}
