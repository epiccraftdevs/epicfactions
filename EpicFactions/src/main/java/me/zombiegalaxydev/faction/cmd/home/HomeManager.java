package me.zombiegalaxydev.faction.cmd.home;
import me.zombiegalaxydev.faction.Core;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class HomeManager {

  private Configuration config;

  public HomeManager () {
    this.config = Core.get ().getConfig ();
  }

  public List<Home> getPlayersHomes(Player player){
    UUID uuid = player.getUniqueId ();
    if (uuid!=null) {
	 ConfigurationSection sec = config.getConfigurationSection (uuid.toString ());
	 if (sec == null) {
	   sec = config.createSection (uuid.toString ());
	 }
	 List<String> stringList = sec.getStringList ("homes");
	 if (stringList == null) {
	   sec.set ("homes", new ArrayList<> ());
	   stringList = new ArrayList<> ();
	 }
	 List<Home> homes = new ArrayList<> ();
	 for (String str : stringList) {
	   String[] data = str.split ("\\|");
	   homes.add (new Home (data[ 0 ], (new Location (Bukkit.getWorld (data[ 1 ]), Double.parseDouble (data[ 2 ]), Double.parseDouble (data[ 3 ]), Double.parseDouble (data[ 4 ])))));
	 }
	 return homes;
    }
    return null;
  }

  public Home getPlayersHome(Player player, String name){
    for (Home home : getPlayersHomes (player)){
	 if (home.getName ().equalsIgnoreCase (name)){
	   return home;
	 }
    }
    return null;
  }

  public void removePlayersHome(final Player player, final String name){
    new BukkitRunnable () {
	 @Override
	 public void run () {
	   try {
		ConfigurationSection sec = config.getConfigurationSection (player.getUniqueId ().toString ());
		if (sec == null) {
		  sec = config.createSection (player.getUniqueId ().toString ());
		}
		List<String> homes = new ArrayList<> ();
		for (Home home : getPlayersHomes (player)){
		  if (home.getName ().equalsIgnoreCase (name))continue;
		  homes.add (home.getConfigLine ());
		}

		homes.remove (new Home (name, new Location (player.getWorld (), player.getLocation ().getX (), player.getLocation ().getY (), player.getLocation ().getZ ())).getConfigLine ());
		sec.set ("homes", homes);
		Core.get ().saveConfig ();
	   } catch (Exception ignored) {
	   }
	 }
    }.runTaskAsynchronously (Core.get ());

  }


  public void addPlayersHome(final Player player, final String name){
    new BukkitRunnable () {
	 @Override
	 public void run () {
	   try {
		ConfigurationSection sec = config.getConfigurationSection (player.getUniqueId ().toString ());
		if (sec == null) {
		  sec = config.createSection (player.getUniqueId ().toString ());
		}
		List<String> homes = new ArrayList<> ();
		for (Home home : getPlayersHomes (player)){
		  homes.add (home.getConfigLine ());
		}
		homes.add (new Home (name, new Location (player.getWorld (), player.getLocation ().getX (), player.getLocation ().getY (), player.getLocation ().getZ ())).getConfigLine ());
		sec.set ("homes", homes);
		Core.get ().saveConfig ();
	   } catch (Exception ignored) {
	   }
	 }
    }.runTaskAsynchronously (Core.get ());
  }

}
