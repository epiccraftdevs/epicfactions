package me.zombiegalaxydev.faction.cmd;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.massivecore.ps.PS;
import me.zombiegalaxydev.faction.Core;
import me.zombieghostdev.gameapi.client.Client;
import me.zombieghostdev.gameapi.client.ClientManager;
import me.zombieghostdev.gameapi.client.Rank;
import net.minecraft.server.v1_8_R3.IInventory;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

public class FactionCommands implements CommandExecutor{

  private static HashMap<UUID, Long> wildernessCooldown = new HashMap<> ();

  public boolean onCommand (CommandSender commandSender, Command command, String s, String[] strings) {
    if (!(commandSender instanceof Player)){
	 return false;
    }
     final Player p = (Player) commandSender;
    if (command.getName ().equalsIgnoreCase ("kit")){
	 Core.get ().getKitManager ().openKitGUI (p);
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("shop")){
	 p.openInventory (Core.get ().getShopManager ().shopMenu ());
	 p.sendMessage("§6§l<!>§e You opened the shop menu!");
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("fly")){
	 Client client = ClientManager.get ().get (p);
	 if (client.getRank ().ordinal ()< Rank.EMERALD.ordinal ()){
	   p.sendMessage ("§4§l<!>§c You need the " + Rank.EMERALD.color  + "EMERALD§c rank tp fly");
	   return true;
	 }
	 if (p.getAllowFlight ()){
	   p.setFlying (false);
	   p.setAllowFlight (false);
	 }else {
	   p.setAllowFlight (true);
	   p.setFlying (true);

	 }

	 p.sendMessage ("§2§l<!>§a Flying " + (p.isFlying ()?"Enabled":"Disabled") + "!");
	 p.playSound (p.getLocation (), Sound.ENDERDRAGON_WINGS, 1, 1);
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("echest")){
	 Client client = ClientManager.get ().get (p);
	 if (client.getRank ().ordinal ()< Rank.DIAMOND.ordinal ()){
	   p.sendMessage ("§4§l<!>§c You need the " + Rank.DIAMOND.color  + "DIAMOND§c rank tp fly");
	   return true;
	 }
	 IInventory inventory = ((CraftPlayer) p).getHandle ().getEnderChest ();
	 ((CraftPlayer) p).getHandle ().openContainer (inventory);
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("trash")){
	 Client client = ClientManager.get ().get (p);
	 if (client.getRank ().ordinal ()< Rank.DIAMOND.ordinal ()){
	   p.sendMessage ("§4§l<!>§c You need the " + Rank.DIAMOND.color  + "DIAMOND§c rank tp fly");
	   return true;
	 }
	 p.openInventory (Bukkit.createInventory (p, 36, "Trash"));
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("wild")){
	 if (!BoardColl.get ().getFactionAt (PS.valueOf (p.getLocation ())).equals (
		FactionColl.get ().getSafezone ()
	 )){
	   p.sendMessage ("§4§l<!>§c You must be in §6SafeZone§c to use this command!");
	   return true;
	 }
	 if (wildernessCooldown.get (p.getUniqueId ())==null || wildernessCooldown.get (p.getUniqueId ()) < System.currentTimeMillis ()){
	   new BukkitRunnable () {
		@Override
		public void run () {
		  p.sendMessage ("§2§l<!>§a Searching for the perfect spot for you.");
		  Location spawnLoc = null;
		  for (int i = 0; i < 1000; i++) {
		    Random r = new Random ();
		    org.bukkit.World world = Bukkit.getWorld ("Faction");
		    Location testLoc = new Location(world,
			   r.nextInt (5000)-r.nextInt (5000), 255,
			   r.nextInt (5000)-r.nextInt (5000));
		    while (testLoc.getBlockY() >= 4) {
			 if (testLoc.getBlock().getType() != Material.AIR) break;
			 testLoc.add(0, -1, 0);
		    }
		    if (testLoc.getBlock().getType() == Material.AIR || testLoc.getBlock().getType() == Material.CACTUS || testLoc.getBlock().getType() == Material.WATER || testLoc.getBlock().getType() == Material.STATIONARY_WATER || testLoc.getBlock().getType() == Material.LAVA || testLoc.getBlock().getType() == Material.STATIONARY_LAVA) {
			 continue;
		    }
		    spawnLoc = testLoc;
		  }
		  p.teleport (spawnLoc);
		  wildernessCooldown.put (p.getUniqueId (), System.currentTimeMillis () + (3 * (60 * 60) * 1000));
		  p.sendMessage ("§2§l<!>§a You teleported into the wilderness!");
		}
	   }.runTask (Core.get ());

	   return true;
	 }
	 long l = wildernessCooldown.get (p.getUniqueId ()) - System.currentTimeMillis ();
	 p.sendMessage ("§4§l<!>§c You cannot use this command for " + Core.timerTans (l/1000));
	 return true;
    }
    return false;
  }
}
