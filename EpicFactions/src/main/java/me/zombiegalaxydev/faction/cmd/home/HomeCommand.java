package me.zombiegalaxydev.faction.cmd.home;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPlayerColl;
import com.massivecraft.massivecore.ps.PS;
import me.zombiegalaxydev.faction.Core;
import me.zombieghostdev.gameapi.client.ClientManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class HomeCommand implements CommandExecutor {

  /**
   * @author Zombie
   *
   * @param commandSender user executing the command.
   * @param command being executed.
   * @param s command line.
   * @param strings arguments.
   * @return executed command.
   */
  @Override
  public boolean onCommand (CommandSender commandSender, Command command, String s, final String[] strings) {
    HomeManager manager = Core.get ().getHomeManager ();
    if (!(commandSender instanceof Player)){
	 return false;
    }
    final Player p = (Player) commandSender;
    if (command.getName ().equalsIgnoreCase ("home")){
	 if (strings.length<=0){
	   p.chat ("/homes");
	   return false;
	 }
	 command.setUsage ("§4§l>>§c/home [name]");

	 if (manager.getPlayersHome (p, strings[0])==null){
	   p.sendMessage ("§4§l<!>§c The home " + strings[0] + " doesn't exist!");
	   return false;
	 }
	 final Location home = manager.getPlayersHome (p, strings[0]).getLocation ();
	 final Location prev = p.getLocation ().clone ();

	 Faction f = MPlayerColl.get ().get (p).getFaction ();
	 if (!f.isNone ()) {
	   PS ps = PS.valueOf (home);
	   if (!BoardColl.get ().getFactionAt (ps).isNone ()
		  && BoardColl.get ().getFactionAt (ps).getRelationTo (f).getValue ()<40) {
		p.sendMessage ("§4§l<!>§c You cannot go to homes in this faction's territory!");
		return true;
	   }
	 }
	 new BukkitRunnable () {
	   int ticks = 5;
	   @Override
	   public void run () {
		if (ticks>0){
		  p.sendMessage ("§2§l<?>§a Teleporting to " + strings[0] + " in " + ticks + " seconds!");
		  if (((int)prev.getX ())!=((int) p.getLocation ().getX ()) || ((int)prev.getZ ())!=((int) p.getLocation ().getZ ())){
		    this.cancel ();
		    p.sendMessage ("§4§l<!>§c You moved and teleporting to " + strings[0] + " has canceled!");
		    return;
		  }
		  ticks--;
		}else {
		  new BukkitRunnable () {
		    @Override
		    public void run () {
			 p.teleport (home.clone ().add (0, .5, 0));
		    }
		  }.runTask (Core.get ());
		  this.cancel ();
		}
	   }
	 }.runTaskTimerAsynchronously (Core.get (), 0L, 20L);
	 return true;
    }else if (command.getName ().equals ("sethome")){
	 command.setUsage ("§4§l>>§c/sethome [home]");
	 if (strings.length<=0){
	   return false;
	 }
	 int max = 2 + (ClientManager.get ().get (p).getRank ().ordinal ());
	 if (manager.getPlayersHomes (p).size ()>=max){
	   p.sendMessage ("§4§l<!>§c You have a maximum of " + max + " homes.");
	   return true;
	 }
	 manager.addPlayersHome (p, strings[0]);
	 p.sendMessage ("§6§l<!>§e You set home " + strings[0]);
	 return true;
    }else if (command.getName ().equals ("delhome")){
	 command.setUsage ("§4§l>>§c/dethome [home]");
	 if (strings.length<=0){
	   return false;
	 }
	 if (manager.getPlayersHome (p, strings[0])==null){
	   p.sendMessage ("§4§l<!>§c Home " + strings[0] + " doesn't exist!");
	 }else {
	   manager.removePlayersHome (p, strings[0]);
	   p.sendMessage ("§6§l<!>§c You deleted home " + strings[0]);
	 }
	 return true;
    }else if (command.getName ().equals ("homes")){
	 List<Home> homes = manager.getPlayersHomes (p);
	 if (homes == null || homes.isEmpty ()){
	   p.sendMessage ("§4§l<!>§c You have no homes!");
	   return true;
	 }else {
	   StringBuilder builder = new StringBuilder ().append ("§6§l>>§e Your homes\n");
	   for (Home home : homes){
		builder.append (" §6§l- §e").append (home.getName ()).append ("\n");
	   }
	   p.sendMessage (builder.toString ());
	 }
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("spawn")){
	 final Location prev = p.getLocation ();
	 new BukkitRunnable () {
	   int ticks = 5;
	   @Override
	   public void run () {
		if (ticks>0){
		  p.sendMessage ("§2§l<?>§a Teleporting to spawn in " + ticks + " seconds!");
		  if (((int)prev.getX ())!=((int) p.getLocation ().getX ()) || ((int)prev.getZ ())!=((int) p.getLocation ().getZ ())){
		    this.cancel ();
		    p.sendMessage ("§4§l<!>§c You moved and teleporting to " + strings[0] + " has canceled!");
		    return;
		  }
		  ticks--;
		}else {
		  new BukkitRunnable () {
		    @Override
		    public void run () {
			 p.teleport (Bukkit.getWorld ("faction").getSpawnLocation ().clone ().add (.5, .5, .5));
		    }
		  }.runTask (Core.get ());
		  this.cancel ();
		}
	   }
	 }.runTaskTimerAsynchronously (Core.get (), 0L, 20L);
	 return true;
    }
    return false;
  }
}
