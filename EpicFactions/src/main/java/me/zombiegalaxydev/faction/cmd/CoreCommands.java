package me.zombiegalaxydev.faction.cmd;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;
import me.zombiegalaxydev.faction.Core;
import me.zombiegalaxydev.faction.utils.CoreManager;
import me.zombiegalaxydev.faction.utils.FactionItems;
import net.ess3.api.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.math.BigDecimal;

public class CoreCommands implements CommandExecutor{

  @Override
  public boolean onCommand (final CommandSender commandSender, Command command, String s, final String[] strings) {
    if (command.getName ().equalsIgnoreCase ("fix")){
	 if (!(commandSender instanceof Player)){
	   return false;
	 }
	 Player p = (Player) commandSender;
	 ItemStack item = p.getItemInHand ();
	 if (item==null){
	   p.sendMessage ("§4§l<!>§c This item can not be repaired");
	   return true;
	 }
	 double money = 0;
	 try {
	   money = Economy.getMoney (p.getName ());
	 } catch (UserDoesNotExistException e) {
	   e.printStackTrace ();
	 }
	 if (money<5000){
	   p.sendMessage ("§4§l<!>§c You need 5k to repair items.  §c(-5000)");
	   return true;
	 }
	 try {
	   Economy.substract (p.getName (), BigDecimal.valueOf (5000));
	   item.setDurability ((short) 0);
	   p.sendMessage ("§2§l<!>§a You repaired this item!");
	 } catch (UserDoesNotExistException | NoLoanPermittedException e) {
	   e.printStackTrace ();
	   p.sendMessage ("§4§l<!>§c Error occurred, " + e.getMessage ());
	 }
	 return true;
    }
    else if (command.getName ().equalsIgnoreCase ("feed")){
	 if (!(commandSender instanceof Player)){
	   return false;
	 }
	 Player p = (Player) commandSender;
	 double money = 0;
	 try {
	   money = Economy.getMoney (p.getName ());
	 } catch (UserDoesNotExistException e) {
	   e.printStackTrace ();
	 }
	 if (money<1000){
	   p.sendMessage ("§4§l<!>§c You need 1k auto eat.");
	   return true;
	 }
	 try {
	   Economy.substract (p.getName (), BigDecimal.valueOf (1000));
	   p.setFoodLevel (20);
	   p.setSaturation (13);
	   p.sendMessage ("§2§l<!>§a You ate at Mc'Epics! §c(-1000)");
	 } catch (UserDoesNotExistException | NoLoanPermittedException e) {
	   e.printStackTrace ();
	   p.sendMessage ("§4§l<!>§c Error occurred, " + e.getMessage ());
	 }
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("note")){
	 if (!(commandSender instanceof Player)){
	   return false;
	 }
	 Player p = (Player) commandSender;
	 if (!p.isOp ()){
	   p.sendMessage ("§4§l<!>§c Operator command only!");
	   return true;
	 }
	 p.getInventory ().addItem (FactionItems.getMoneyNote (Double.parseDouble (strings[0])));
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("ping")) {
	 if (! (commandSender instanceof Player)) {
	   return false;
	 }
	 new BukkitRunnable () {
	   @Override
	   public void run () {
		Player p = (Player) commandSender;
		if (strings.length < 1 || Bukkit.getPlayer (strings[ 0 ]) == null) {
		  p.sendMessage ("§2§l<!>§a Your ping§7: " + ((CraftPlayer) p).getHandle ().ping + ".");
		} else {
		  p.sendMessage ("§2§l<!>§a " + strings[ 0 ] + " ping§7: " + ((CraftPlayer) Bukkit.getPlayer (strings[ 0 ])).getHandle ().ping + ".");
		}
	   }
	 }.runTaskAsynchronously (Core.get ());
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("near")) {
	 if (! (commandSender instanceof Player)) {
	   return false;
	 }
	 Player p = (Player) commandSender;
	 StringBuilder builder = new StringBuilder ();
	 String string = "none";
	 for (Entity entity : p.getNearbyEntities (250, 250, 250)){
	   if (entity instanceof Player){
		if (p.spigot ().getHiddenPlayers ().contains (entity))continue;
		builder.append (entity.getName ()).append (" ").append ("§4(§c").append (entity.getLocation ().distance (p.getLocation ())).append ("§4) ");
	   }
	 }
	 if (builder.length ()>0){
	   string = builder.toString ();
	 }
	 p.sendMessage ("§6§l>>§e near:§f " + string);
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("stack")) {
	 if (! (commandSender instanceof Player)) {
	   return false;
	 }
	 Player p = (Player) commandSender;
	 ItemStack item = p.getItemInHand ();
	 if (item==null||item.getType ().equals (Material.AIR)){
	   p.sendMessage ("§4§l<!>§c You can't stack air.");
	   return true;
	 }
	 int amount = item.getAmount () - 1;
	 for (ItemStack it : p.getInventory ().getContents ()){
	   if (amount>=16)break;
	   if (it==null || it.getType () == null||it.getType ().equals (Material.AIR))continue;
	   if (!item.getType ().equals (it.getType ()))continue;
	   if (!item.getData ().equals (it.getData ()))continue;
	   p.getInventory ().remove (it);
	   amount++;
	 }
	 p.setItemInHand (new ItemStack (item.getType (), amount, item.getDurability ()));
	 return true;
    }else if (command.getName ().equalsIgnoreCase ("balance")) {
	 if (! (commandSender instanceof Player)) {
	   return false;
	 }
	 new BukkitRunnable () {
	   @Override
	   public void run () {
		Player p = (Player) commandSender;
		if (strings.length < 1 || Bukkit.getPlayer (strings[ 0 ]) == null) {
		  try {
		    p.sendMessage ("§2§l<!>§a Your balance§7: " + CoreManager.formatMoney (Economy.getMoney (p.getName ())) + ".");
		  } catch (UserDoesNotExistException e) {
		    p.sendMessage ("§4§l<!>§c " + e.getMessage ());
		  }
		} else {
		  try {
		    p.sendMessage ("§2§l<!>§a " + strings[0] + " balance§7: " + CoreManager.formatMoney (Economy.getMoney (strings[0])) + ".");
		  } catch (UserDoesNotExistException e) {
		    p.sendMessage ("§4§l<!>§c " + e.getMessage ());
		  }		}
	   }
	 }.runTaskAsynchronously (Core.get ());
	 return true;
    }
    return false;
  }
}
