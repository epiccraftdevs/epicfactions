package me.zombiegalaxydev.faction;
import me.zombiegalaxydev.faction.channel.command.ChatManager;
import me.zombiegalaxydev.faction.cmd.CoreCommands;
import me.zombiegalaxydev.faction.cmd.FactionCommands;
import me.zombiegalaxydev.faction.cmd.home.HomeCommand;
import me.zombiegalaxydev.faction.cmd.home.HomeManager;
import me.zombiegalaxydev.faction.enchants.CustomEnchantListener;
import me.zombiegalaxydev.faction.enchants.CustomEnchantManager;
import me.zombiegalaxydev.faction.kits.KitManager;
import me.zombiegalaxydev.faction.utils.CoreManager;
import me.zombiegalaxydev.faction.utils.CrateManager;
import me.zombiegalaxydev.faction.utils.HeadManager;
import me.zombiegalaxydev.faction.utils.ShopManager;
import me.zombieghostdev.gameapi.GameAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Core extends JavaPlugin {

  private static Core instance;
  private KitManager kitManager;
  private ShopManager shopManager;
  private HomeManager homeManager;
  private CustomEnchantManager customEnchantManager;

  public KitManager getKitManager () {
    return kitManager;
  }

  public ShopManager getShopManager () {
    return shopManager;
  }

  public HomeManager getHomeManager () {
    return homeManager;
  }

  public CustomEnchantManager getCustomEnchantManager () {
    return customEnchantManager;
  }

  public static Core get () {
    return instance;
  }

  @Override
  public void onEnable () {
    super.onEnable ();
    instance = this;
    kitManager = new KitManager ();
    shopManager = new ShopManager ();
    homeManager = new HomeManager ();
    customEnchantManager = new CustomEnchantManager ();

    getCommand ("kit").setExecutor (new FactionCommands ());
    getCommand ("shop").setExecutor (new FactionCommands ());
    getCommand ("fly").setExecutor (new FactionCommands ());
    getCommand ("echest").setExecutor (new FactionCommands ());
    getCommand ("trash").setExecutor (new FactionCommands ());
    getCommand ("chat").setExecutor (new ChatManager ());

    Bukkit.getPluginManager ().registerEvents (new CoreManager (), this);
    Bukkit.getPluginManager ().registerEvents (new CrateManager (), this);
    Bukkit.getPluginManager ().registerEvents (new CustomEnchantListener (), this);
    Bukkit.getPluginManager ().registerEvents (new HeadManager (), this);
    Bukkit.getPluginManager ().registerEvents (kitManager, this);

    HomeCommand homeCommand = new HomeCommand ();
    getCommand ("home").setExecutor (homeCommand);
    getCommand ("sethome").setExecutor (homeCommand);
    getCommand ("delhome").setExecutor (homeCommand);
    getCommand ("homes").setExecutor (homeCommand);
    getCommand ("spawn").setExecutor (homeCommand);

    getCommand ("fix").setExecutor (new CoreCommands ());
    getCommand ("feed").setExecutor (new CoreCommands ());
    getCommand ("note").setExecutor (new CoreCommands ());
    getCommand ("ping").setExecutor (new CoreCommands ());
    getCommand ("near").setExecutor (new CoreCommands ());
    getCommand ("stack").setExecutor (new CoreCommands ());
    getCommand ("balance").setExecutor (new CoreCommands ());
    getCommand ("sellheads").setExecutor (new HeadManager ());
/*
    new BukkitRunnable () {
	 int ticks = 60*4;
	 @Override
	 public void run () {
	   if (Calendar.getInstance ().getTime ().getHours ()%11==0&&
		  Calendar.getInstance ().getTime ().getMinutes ()>=56) {
		if (ticks%30==0) {
		  Bukkit.broadcastMessage (
			 "§8§m-----------------------------§r \n" +
				" §4§l<!!!> §c§lServer Restarts in §f§l" + timerTans (ticks) + "§c§l! §4§l<!!!>\n" +
				" §8§m-----------------------------§r");
		}
		if (ticks <= 0) {
		  Bukkit.reload ();
		}
		ticks--;
	   }
	 }
    }.runTaskTimerAsynchronously (this, 0L, 20L);*/
  }

  @Override
  public void onDisable () {
    for (Player p : Bukkit.getOnlinePlayers ()){
	 GameAPI.sendToServer (p, "Hub");
    }
  }

  public static String timerTans(long ticks){

    long days = ticks/86400;
    long hours = (ticks/3600-(days*24));
    long remainder = ticks%3600;
    long mins = remainder/60;
    long secs = remainder%60;

    StringBuilder builder = new StringBuilder ();
    if (days>0){
	 builder.append (days).append ("d");
    } if (hours>0){
	 builder.append (hours).append ("h");
    } if (mins>=0){
	 builder.append (mins).append ("m");
    } if (secs>=0){
	 builder.append (secs).append ("s");
    }
    return builder.toString ();
  }
}
