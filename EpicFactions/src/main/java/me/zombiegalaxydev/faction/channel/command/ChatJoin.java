package me.zombiegalaxydev.faction.channel.command;
import me.zombiegalaxydev.faction.channel.ChatChannel;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class ChatJoin extends ChatCommand {

  /**
   * @author Zombie
   *
   * @description ChatCommand "Join" constructor
   */
  public ChatJoin () {
    super ("join");
  }

  /**
   * @author Zombie
   *
   * @param sender is the user activating the command.
   * @param strings	is the possible arguments for the command.
   */
  @Override
  public boolean executor (CommandSender sender, String[] strings) {
    if (strings.length<=1){
	 sender.sendMessage ("§4§l<!>§c You need to include a channel." + "\n" + "§6§l>>§c/c ls");
	 return true;
    }
    String string = strings[1];
    ChatChannel channel = ChatChannel.valueOf (string.toUpperCase ());
    if (channel.equals (null)){
	 sender.sendMessage ("§4§l<!>§c " + string + " is not a channel." + "\n" + "§6§l>>§c/c ls");
	 return true;
    }
    ChatChannel.setUserChannel ((Player) sender, channel);
    sender.sendMessage ("§2§l<!>§a You joined channel " + channel.name () + "!");
    return true;
  }
}
