package me.zombiegalaxydev.faction.channel.command;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class  ChatManager implements CommandExecutor {

  /**
   * @author Zombie
   *
   * @description Initializes ChatCommands.
   */
  public ChatManager () {
    ChatCommand.get ().add (new ChatHelp ());
    ChatCommand.get ().add (new ChatList ());
    ChatCommand.get ().add (new ChatJoin ());
  }

  /**
   * @author Zombie
   *
   * @param commandSender the user activating the command.
   * @param command	the command its self.
   * @param s string of command sent.
   * @param strings	possible arguments for the command.
   * @return chat command execution.
   */
  @SuppressWarnings("ConstantConditions")
  @Override
  public boolean onCommand (CommandSender commandSender, Command command, String s, String[] strings) {
    if (command.getName ().equalsIgnoreCase ("chat")){
	 command.setUsage ("§6§l<!>§e /chat [command]");
	 if (strings.length<=0){
	   commandSender.sendMessage ("§4§l<!>§c Too little arguments.");
	   return false;
	 }
	 ChatCommand chatCommand = ChatCommand.get (strings[0]);
	 if (chatCommand==null){
	   commandSender.sendMessage ("§4§l<!>§c Command §o" + strings[0] + "§c not found.");
	   return ChatCommand.get ("help").executor (commandSender, strings);
	 }
	 return chatCommand.executor (commandSender, strings);
    }
    return false;
  }
}
