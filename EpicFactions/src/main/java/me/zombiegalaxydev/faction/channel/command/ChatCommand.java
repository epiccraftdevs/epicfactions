package me.zombiegalaxydev.faction.channel.command;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class ChatCommand {

  private static List<ChatCommand> chatCommands = new ArrayList<> ();	//List of commands

  private String string;	//Command Name
  private List<String> aliases = new ArrayList<> ();	//Aliases for the command

  /**
   * @author Zombie
   *
   * @param string is possibly a command in the list.
   * @return ChatCommand that matches the string.
   */
  public static ChatCommand get (String string){
    for (ChatCommand chatCommand : chatCommands){
	 if (!chatCommand.getString ().equalsIgnoreCase (string))continue;
	 return chatCommand;
    }
    return null;
  }

  /**
   * @author Zombie
   *
   * @return List of the chatCommands.
   */
  public static List<ChatCommand> get () {
    return chatCommands;
  }

  /**
   * @author Zombie
   *
   * @param string is the command name.
   * @param aliases is the aliases for the command.
   */
  public ChatCommand (String string, String... aliases) {
    this.string = string;
    Collections.addAll (this.aliases, aliases);
  }

  /**
   * @author Zombie
   *
   * @return command name.
   */
  public String getString () {
    return string;
  }

  /**
   * @author Zombie
   *
   * @return Aliases for the command.
   */
  public List<String> getAliases () {
    return aliases;
  }

  /**
   * @author Zombie
   *
   * @param sender is the user activating the command.
   * @param strings	is the possible arguments for the command.
   */
  public abstract boolean executor(CommandSender sender, String[] strings);
}
