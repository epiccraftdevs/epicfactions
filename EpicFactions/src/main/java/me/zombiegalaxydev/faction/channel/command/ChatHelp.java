package me.zombiegalaxydev.faction.channel.command;
import org.bukkit.command.CommandSender;

public class ChatHelp extends ChatCommand {

  /**
   * @author Zombie
   *
   * @description ChatCommand "Help" constructor
   */
  public ChatHelp () {
    super ("help");
  }

  /**
   * @author Zombie
   *
   * @param sender is the user activating the command.
   * @param strings	is the possible arguments for the command.
   */
  @Override
  public boolean executor (CommandSender sender, String[] strings) {
    sender.sendMessage ("§7§m----------§r§7{§e§lChat Channel Help§7}§m----------");
    for (ChatCommand command : ChatCommand.get ()){
	 if (!command.getAliases ().isEmpty ()) {
	   StringBuilder builder = new StringBuilder ().append (" §8[§7");
	   for (String alias : command.getAliases ()) {
		builder.append (alias).append (",");
	   }
	   sender.sendMessage ("§7/chat " + command.getString () + builder.append ("§8]").toString ());
	 }else {
	   sender.sendMessage ("§7/chat " + command.getString ());
	 }
    }
    sender.sendMessage ("§7§m------------------------------------");
    return true;
  }
}
