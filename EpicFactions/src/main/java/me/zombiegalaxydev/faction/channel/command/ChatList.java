package me.zombiegalaxydev.faction.channel.command;
import me.zombiegalaxydev.faction.channel.ChatChannel;
import org.bukkit.command.CommandSender;

public class ChatList extends ChatCommand {

  /**
   * @author Zombie
   *
   * @description ChatCommand "List" constructor
   */
  public ChatList () {
    super ("list", "ls");
  }

  /**
   * @author Zombie
   *
   * @param sender is the user activating the command.
   * @param strings	is the possible arguments for the command.
   */
  @Override
  public boolean executor (CommandSender sender, String[] strings) {
    StringBuilder builder = new StringBuilder ().append ("§7§m----------§r§7{§e§lChat Channel List§7}§m----------").append ("\n");
    for (ChatChannel channel : ChatChannel.values ()){
	 builder.append (" §7- ").append (channel.name ()).append (" ").append (channel.getPrefix ()).append ("\n");
    }
    sender.sendMessage (builder.append ("§7§m------------------------------------").toString ());
    return true;
  }
}
