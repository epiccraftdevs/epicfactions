package me.zombiegalaxydev.faction.channel;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public enum ChatChannel {
  ALLY("§d§lA", ChatColor.LIGHT_PURPLE), FACTION("§a§lF", ChatColor.GREEN), GLOBAL("§f§lG", ChatColor.WHITE), WHISPER("§8§lW", ChatColor.GRAY);

  private static HashMap<UUID, ChatChannel> userChannel = new HashMap<> (); //User Channels

  /**
   * @author Zombie
   *
   * @param player seeking user channel.
   * @return player's channel.
   */
  public static ChatChannel getUserChannel (Player player){
    for (ChatChannel chatChannel : ChatChannel.values ()) {
	 if (!userChannel.containsKey (player.getUniqueId ())){
	   return userChannel.put (player.getUniqueId (), GLOBAL);
	 }
	 if (!userChannel.get (player.getUniqueId ()).equals (chatChannel)) continue;
	 return chatChannel;
    }
    return userChannel.put (player.getUniqueId (), GLOBAL);
  }

  /**
   * @author Zombie
   *
   * @param player user setting the channel for.
   * @param chatChannel selected.
   */
  public static void setUserChannel(Player player, ChatChannel chatChannel){
    userChannel.put (player.getUniqueId (), chatChannel);
  }

  private String prefix;	//Channel and Prefix string.
  private ChatColor color;

  /**
   * @author Zombie
   *
   * @param prefix for the channel.
   */
  ChatChannel (String prefix, ChatColor color) {
    this.prefix = prefix;
    this.color = color;
  }

  /**
   * @author Zombie
   *
   * @return prefix for channel.
   */
  public String getPrefix () {
    return prefix;
  }

  /**
   * @author Zombie
   *
   * @return color for channel.
   */
  public ChatColor getColor () {
    return color;
  }
}
