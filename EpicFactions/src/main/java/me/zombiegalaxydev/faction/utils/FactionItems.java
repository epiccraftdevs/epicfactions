package me.zombiegalaxydev.faction.utils;
import me.zombiegalaxydev.faction.kits.FactionKit;
import me.zombieghostdev.gameapi.utils.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class FactionItems {

  /**
   * @author Zombie
   *
   * @param amount of money attached to the note.
   * @return a Money note.
   */
  public static ItemStack getMoneyNote(double amount){
    return new ItemStackBuilder (Material.PAPER).getUnsafeEnchantment (Enchantment.LUCK, (int) amount).getDisplayName ("§a§lMoney Note §7§o(Right Click to Redeem)").getLore ("§2Value§7:§f " + CoreManager.formatMoney (amount)).getNBTTag ("money", amount).build ();
  }

  /**
   * @author Zombie
   *
   * @param kit kit attached to the note
   * @return a Money note.
   */
  public static ItemStack getKitNote(FactionKit kit){
    return new ItemStackBuilder (Material.PAPER).getUnsafeEnchantment (Enchantment.LUCK, 1).getDisplayName ("§a§lKit Note §7§o(Right Click to Redeem)").getLore ("§2Kit§7:§f " + kit.name ()).getNBTTag ("kit", kit.name ()).build ();
  }


  /**
   * @author Zombie
   *
   * @param amount of levels attached to the note.
   * @param skill The skill attached for note.
   * @return a MCMMO note.
   */
  public static ItemStack getMCMMONote(int amount, String skill){
    return new ItemStackBuilder (Material.PAPER).getUnsafeEnchantment (Enchantment.LURE, amount).getDisplayName ("§a§lMCMMO Note §7§o(Right Click to Redeem").getLore ("§2Value§7:§f " + amount + "XP").getNBTTag ("mcmmo", amount).getNBTTag ("skill", skill).build ();
  }
}
