package me.zombiegalaxydev.faction.utils;
import com.massivecraft.factions.Rel;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.factions.entity.MPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class FactionUtil {

  public static List<Player> getListPlayerRole(Faction faction, Rel... rels){
    List<Player> players = new ArrayList<> ();
    for (Rel rel : rels){
	 for (MPlayer mplayer : faction.getMPlayersWhereRole (rel)){
	   if (mplayer.isOnline ()){
		players.add (mplayer.getPlayer ());
	   }
	 }
    }
    return players;
  }

  public static Rel relationWith(Faction faction, Player player){
    FactionColl.get ().reindexMPlayers ();
    for (Rel rel : Rel.values ()) {
	 for (MPlayer mp : faction.getMPlayersWhereRole (rel)) {
	   if (mp.getUuid ().equals (player.getUniqueId ())){
		return rel;
	   }
	 }
    }
    return Rel.NEUTRAL;
  }

}
