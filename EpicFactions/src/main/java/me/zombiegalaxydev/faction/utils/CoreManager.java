package me.zombiegalaxydev.faction.utils;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;
import com.gmail.nossr50.api.ExperienceAPI;
import com.massivecraft.factions.Rel;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.factions.entity.MPlayerColl;
import com.massivecraft.factions.event.EventFactionsCreate;
import com.massivecraft.massivecore.ps.PS;
import me.zombiegalaxydev.faction.Core;
import me.zombiegalaxydev.faction.channel.ChatChannel;
import me.zombiegalaxydev.faction.kits.FactionKit;
import me.zombiegalaxydev.faction.kits.KitManager;
import me.zombiegalaxydev.faction.kits.StarterKit;
import me.zombiegalaxydev.faction.utils.events.KitUseEvent;
import me.zombieghostdev.gameapi.GameAPI;
import me.zombieghostdev.gameapi.client.Client;
import me.zombieghostdev.gameapi.client.ClientManager;
import me.zombieghostdev.gameapi.utils.ItemStackBuilder;
import me.zombieghostdev.gameapi.utils.itemnbtapi.NBTItem;
import me.zombieghostdev.gameapi.utils.scoreboard.ScoreboardBuilder;
import net.ess3.api.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.*;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CoreManager implements Listener {

  public CoreManager () {
    {
	 new BukkitRunnable () {
	   int ticks = 900;
	   @Override
	   public void run () {
		ticks--;
		if (ticks==60 || ticks == 20){
		  Bukkit.broadcastMessage ("§4§l<!>§c Entities will clear in §f§l" + ticks + "§c seconds!");
		}else if (ticks<=0){
		  new BukkitRunnable () {
		    @Override
		    public void run () {
			 int intE = 0;
			 for (World w : Bukkit.getWorlds ()){
			   for (Entity e : w.getEntities ()){
				if (e.isOnGround () && !(e instanceof Player) && !(e instanceof ArmorStand) && !(e instanceof ItemFrame) && !(e instanceof FallingBlock)){
				  e.remove (); intE++;
				}

			   }
			 }
			 Bukkit.broadcastMessage ("§4§l<!>§f§l " + intE + "§c entities were removed!");
		    }
		  }.runTask (Core.get ());
		  ticks=900;
		}
	   }
	 }.runTaskTimerAsynchronously (Core.get (), 0L, 20L);
    }
  }

  public static String formatMoney (double userMoney){
    StringBuilder builder = new StringBuilder ();
    DecimalFormat df = new DecimalFormat ("#.##");
    if (userMoney >= 1000000){
	 double money = userMoney / 1000000;
	 builder.append (df.format (money)).append ("m");
    }else if (userMoney >= 1000){
	 double money = userMoney / 1000;
	 builder.append (df.format (money)).append ("k");
    }else {
	 builder.append (df.format (userMoney)).append ("$");
    }
    return builder.toString ();
  }

  private String formatMoney(Player p){
    try {
	 return formatMoney (Economy.getMoney (p.getName ()));
    } catch (UserDoesNotExistException e) {
	 e.printStackTrace ();
    }
    return null;
  }

  private void update() {
    for (Player p : Bukkit.getOnlinePlayers ()) {
	 if (!p.isOnline ())continue;
	 if (!MPlayerColl.get ().get (p).isOnline ())continue;
	 Faction faction = MPlayerColl.get ().get (p).getFaction ();
	 ScoreboardBuilder builder = new ScoreboardBuilder ("information", "dummy", "§4§lFactions FIRE", 15);
	 builder.getSpace ();
	 builder.getScore ("§cFaction");
	 builder.getScore ("§a" + faction.getName ());
	 builder.getSpace ();
	 builder.getScore ("§cPower");
	 builder.getScore (! faction.getId ().equalsIgnoreCase ("none") ? String.valueOf ((long) faction.getPower ()) : "§7none");
	 builder.getSpace ();
	 builder.getScore ("§cBalance");
	 builder.getScore (formatMoney (p));
	 builder.getSpace ();
	 builder.getScore ("§cChannel");
	 builder.getScore (ChatChannel.getUserChannel (p).name ());
	 if (! faction.getId ().equalsIgnoreCase ("none")) {
	   builder.getSpace ();
	   builder.getScore ("§cRank");
	   builder.getScore (FactionUtil.relationWith (faction, p).name ());
	 }

	 p.setScoreboard (builder.getScoreboard ());
	 GameAPI.sendTabBar (p, "\n §4§l[§c§lFACTIONS FIRE§4§l]\n ", "\n" +
		" §8" + ChatColor.STRIKETHROUGH + "---------------§r" +
		"\n §4§lStatus\n " +
		"\n §c§lPlayers:§f§l " + Bukkit.getOnlinePlayers ().size () +
		"\n §c§lBalance:§f§l " + formatMoney (p) +
		"\n §c§lFaction:§f§l " + MPlayerColl.get ().get (p).getFaction ().getName () +
		"\n §8" + ChatColor.STRIKETHROUGH + "---------------§r \n"
	 );
    }
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onRedStone(final BlockRedstoneEvent e){
    final int i = e.getNewCurrent ();
    e.setNewCurrent (e.getOldCurrent ());
    new BukkitRunnable () {
	 @Override
	 public void run () {
	   e.setNewCurrent (i);
	 }
    }.runTaskAsynchronously (Core.get ());
  }

  @EventHandler
  public void onJoin(final PlayerJoinEvent e){
    ChatChannel.setUserChannel (e.getPlayer (), ChatChannel.GLOBAL);
    e.getPlayer ().addPotionEffect (new PotionEffect (PotionEffectType.DAMAGE_RESISTANCE, 20 * 2, 1000000000));
    new BukkitRunnable () {
	 @Override
	 public void run () {
	   update ();
	 }
    }.runTaskLater (Core.get (), 20L);
    final List<String> messages = new ArrayList<> ();
    messages.add ("§a§lWelcome to Factions §c§lFiRE§a§l!");
    messages.add ("§c§lStore coming soon!");
    messages.add ("§2§lTIP, got mob heads? do /sellheads to sell them!");
    new BukkitRunnable () {
	 int ticks = 25;
	 String message = messages.get (new Random ().nextInt (messages.size ()));
	 @Override
	 public void run () {
	   if (e.getPlayer () == null || !e.getPlayer ().isOnline ()){
		this.cancel ();
		return;
	   }
	   ticks--;
	   if (ticks<=0){
		String newMessage = message;
		while (newMessage.equals (message)){
		  newMessage = messages.get (new Random ().nextInt (messages.size ()));
		}
		message = newMessage;
		ticks=25;
	   }
	   GameAPI.sendActionBar (e.getPlayer (), message);
	 }
    }.runTaskTimerAsynchronously (Core.get (), 0L, 5L);
  }

  @EventHandler
  public void onQuit(PlayerQuitEvent e){
    if (e.getPlayer ().getPassenger ()!=null){
	 e.getPlayer ().getPassenger ().remove ();
    }
  }

  @EventHandler
  public void onAchieve(PlayerAchievementAwardedEvent e){
    e.setCancelled (true);
  }

  //<-- Placing Spawner Listener -->
  @EventHandler(priority = EventPriority.LOWEST)
  public void onPlace(final BlockPlaceEvent e){
    if (e.getItemInHand ().getType ().equals (Material.MOB_SPAWNER)){
	 CreatureSpawner spawner = (CreatureSpawner) e.getBlockPlaced ().getState ();
	 spawner.setSpawnedType (EntityType.valueOf (ChatColor.stripColor (e.getItemInHand ().getItemMeta ().getDisplayName ().split (" ")[0]).toUpperCase ()));
	 spawner.update ();
    }
  }

  //<-- Mining Spawner Listener -->
  @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
  public void onBlock(BlockBreakEvent e){
    Block b = e.getBlock ();
    ItemStack itemInHand = e.getPlayer ().getItemInHand ();
    if (itemInHand!=null&&itemInHand.getEnchantments ().containsKey (Enchantment.SILK_TOUCH)&&b.getType ().equals (Material.MOB_SPAWNER)) {
	 CreatureSpawner spawner = (CreatureSpawner) b.getState ();
	 b.getWorld ().dropItem (b.getLocation (),
		new ItemStack (Material.MOB_SPAWNER)).setItemStack (
		new ItemStackBuilder (
		    new ItemStack (Material.MOB_SPAWNER)).getDisplayName (
		    "§e§l" + (spawner.getCreatureTypeName ().equalsIgnoreCase ("VillagerGolem")?
			   "Iron_Golem":spawner.getCreatureTypeName ().equalsIgnoreCase ("Cave_Spider")?"Cave_Spider":spawner.getCreatureTypeName ()) + "§r Spawner").build ());
	 b.setType (Material.AIR);
	 e.setCancelled (true);
    }
  }

  //<-- Chat Re-Formatting -->
  @SuppressWarnings("ConstantConditions")
  @EventHandler(priority = EventPriority.LOW)
  public void onChat(AsyncPlayerChatEvent e){
    if (e.isCancelled ())return;
    e.setCancelled (true);
    Client client = ClientManager.get ().get (e.getPlayer ());

    Faction pf = MPlayerColl.get ().get (e.getPlayer ()).getFaction ();
    if (pf!=null) {
	 e.getRecipients ().clear ();

	 ChatChannel channel = ChatChannel.getUserChannel (e.getPlayer ());
	 if (channel==null){
	   channel = ChatChannel.GLOBAL;
	 }
	 if (e.getMessage ().split (" ")[ 0 ].contains (":")) {
	   String chat = e.getMessage ().split (" ")[ 0 ].split (":")[ 0 ];
	   if (!chat.equalsIgnoreCase ("")) {
		e.setMessage (e.getMessage ().replaceFirst (chat + ":", " "));
		if (chat.equalsIgnoreCase ("W")) {
		  channel = ChatChannel.WHISPER;
		} else if (chat.equalsIgnoreCase ("A")) {
		  channel = ChatChannel.ALLY;
		} else if (chat.equalsIgnoreCase ("F")) {
		  channel = ChatChannel.FACTION;
		} else if (chat.equalsIgnoreCase ("G")) {
		  channel = ChatChannel.GLOBAL;
		} else {
		  channel = ChatChannel.GLOBAL;
		}
	   }

	   if (e.getMessage ().split (" ").length<1){
		ChatChannel.setUserChannel (e.getPlayer (), channel);
		e.getPlayer ().sendMessage ("§2§l<!>§a You joined channel " + channel.name () + "!");
		e.setCancelled (true);
		return;
	   }

	 }

	 if (channel != null) {
	   switch (channel) {
		case WHISPER:
		  for (Entity entity : e.getPlayer ().getNearbyEntities (50, 50, 50)) {
		    if (entity instanceof Player) {
			 e.getRecipients ().add ((Player) entity);
		    }
		  }
		  e.getRecipients ().add (e.getPlayer ());
		  break;
		case ALLY:
		  for (Player p : FactionUtil.getListPlayerRole (pf, Rel.ALLY, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT)) {
		    e.getRecipients ().add (p);
		  }
		  break;
		case FACTION:
		  for (Player p : FactionUtil.getListPlayerRole (pf, Rel.LEADER, Rel.OFFICER, Rel.MEMBER, Rel.RECRUIT)) {
		    e.getRecipients ().add (p);
		  }
		  break;
		case GLOBAL:
		  for (Player p : Bukkit.getOnlinePlayers ()) {
		    if (p.isOnline ()) {
			 e.getRecipients ().add (p);
		    }
		  }
		  break;
		default:
		  for (Player p : Bukkit.getOnlinePlayers ()) {
		    if (p.isOnline ()) {
			 e.getRecipients ().add (p);
		    }
		  }
		  break;
	   }
	 }

	 try {
	   e.getRecipients ().remove (e.getPlayer ());
	   for (Player p : e.getRecipients ()) {
		if (!MPlayerColl.get ().get (p).getFaction ().isNone () && MPlayerColl.get ().get (p).getFaction ().equals (pf)){
		  p.sendMessage ("§8§l[" + channel.getPrefix () + "§8§l] §8§l[§a" + pf.getName () + "§8§l]§6§l " + FactionUtil.relationWith (pf, e.getPlayer ()).getPrefix () + "§f " + client.getRank ().color + e.getPlayer ().getName () + " §2» " + channel.getColor () + e.getMessage ());
		}else {
		  Rel rel = MPlayerColl.get ().get (p).getFaction ().getRelationWish (pf);
		  switch (rel) {
		    case ALLY:
			 p.sendMessage ("§8§l[" + channel.getPrefix () + "§8§l] §8§l[§d" + pf.getName () + "§8§l]§6§l " + FactionUtil.relationWith (pf, e.getPlayer ()).getPrefix () + "§f " + client.getRank ().color + e.getPlayer ().getName () + " §5» " + channel.getColor () + e.getMessage ());
			 break;
		    case TRUCE: case NEUTRAL:
			 p.sendMessage ("§8§l[" + channel.getPrefix () + "§8§l] §8§l[§7" + pf.getName () + "§8§l]§6§l " + FactionUtil.relationWith (pf, e.getPlayer ()).getPrefix () + "§f " + client.getRank ().color + e.getPlayer ().getName () + " §8» " + channel.getColor () + e.getMessage ());
			 break;
		    case ENEMY:
			 p.sendMessage ("§8§l[" + channel.getPrefix () + "§8§l] §8§l[§c" + pf.getName () + "§8§l]§6§l " + FactionUtil.relationWith (pf, e.getPlayer ()).getPrefix () + "§f " + client.getRank ().color + e.getPlayer ().getName () + " §4» " + channel.getColor () + e.getMessage ());
			 break;
		    default:
			 p.sendMessage ("§8§l[" + channel.getPrefix () + "§8§l] §8§l[§7" + pf.getName () + "§8§l]§6§l " + FactionUtil.relationWith (pf, e.getPlayer ()).getPrefix () + "§f " + client.getRank ().color + e.getPlayer ().getName () + " §8» " + channel.getColor () + e.getMessage ());
			 break;
		  }
		}
	   }
	 } catch (Exception ignored) {
	 }finally {
	   e.getPlayer ().sendMessage ("§8§l[" + channel.getPrefix () + "§8§l] §8§l[§a" + pf.getName () + "§8§l]§6§l " + FactionUtil.relationWith (pf, e.getPlayer ()).getPrefix () + "§f " + client.getRank ().color + e.getPlayer ().getName () + " §2» " + channel.getColor () + e.getMessage ());
	 }
    }
  }

  @EventHandler
  public void onBucket(final PlayerBucketEmptyEvent e){
    final Player p = e.getPlayer ();
    final ItemStack item = p.getItemInHand ();
    if (item.getAmount ()>1){
	 item.setAmount (item.getAmount ()-1);
	 new BukkitRunnable () {
	   @Override
	   public void run () {
		p.getInventory ().addItem (new ItemStack (Material.BUCKET));
		p.setItemInHand (item);
		p.updateInventory ();
	   }
	 }.runTaskLater (Core.get (), 0L);
    }
  }

  //<-- Scoreboard Listeners -->
  @EventHandler(priority = EventPriority.LOWEST)
  public void onMove(PlayerMoveEvent e){
    Player p = e.getPlayer ();
    try {
	 Faction f = MPlayerColl.get ().get (p).getFaction ();

	 if (f!=null) {
	   String faction = "§a" + f.getName ();
	   String power = ! f.getId ().equalsIgnoreCase ("none") ? String.valueOf ((long) f.getPower ()) : "§7none";
	   String money = formatMoney (p);
	   Rel rank = null;

	   if (!f.getId ().equalsIgnoreCase ("none")) {
		rank = FactionUtil.relationWith (f, p);
	   }

	   if (p.getScoreboard () != null) {
		Objective obj = p.getScoreboard ().getObjective (DisplaySlot.SIDEBAR);
		if (obj != null) {
		  if ((obj.getScore (faction).getScore () != 13 ||
			 obj.getScore (power).getScore () != 10 ||
			 obj.getScore (money).getScore () != 7 ||
			 (! f.getId ().equalsIgnoreCase ("none") && obj.getScore (rank != null ? rank.name () : null).getScore () != 1))) {
		    update ();
		  }
		}
	   }
	 }
    }catch (Exception ignored){
    }
    if (p.isFlying ()){
	 Faction faction = BoardColl.get ().getFactionAt (PS.valueOf (p.getLocation ()));
	 Faction f = MPlayerColl.get ().get (p).getFaction ();
	 if (faction==null||f==null)return;
	 if (faction.getRelationWish (f).equals (Rel.ENEMY)){
	   p.setFlying (false);
	   p.sendMessage ("§4§l<!>§c You cannot fly in enemy territory");
	   e.setCancelled (true);
	   return;
	 }
	 if (faction.equals (FactionColl.get ().getWarzone ())){
	   p.setFlying (false);
	   p.sendMessage ("§4§l<!>§c You cannot fly in warzone");
	   e.setCancelled (true);
	 }
    }
  }

  //<-- Shop Listeners -->
  @EventHandler(priority = EventPriority.LOWEST)
  public void onInventory(InventoryClickEvent e){
    Player p = (Player) e.getWhoClicked ();
    ItemStack item = e.getCurrentItem ();
    if (item == null) return;
    if (item.getType ()==Material.AIR)return;
    ShopManager manager = Core.get ().getShopManager ();

    //<-- The Shop Menu -->
    if(e.getInventory ().getSize ()==18){
	 e.setResult (Event.Result.DENY);
	 p.openInventory (manager.getShop (new NBTItem (item).getString ("shop")));
    }

    //<-- The Shop its self -->
    else if (manager.isShop (e.getClickedInventory ())) {
	 e.setResult (Event.Result.DENY);
	 if (p.getInventory ().contains (e.getCurrentItem ()))return;
	 NBTItem nbt = new NBTItem (item);
	 if (e.getCurrentItem ().getItemMeta ().getDisplayName ().equalsIgnoreCase ("§e§lShop Menu")){
	   e.getWhoClicked ().openInventory (manager.shopMenu ());
	   return;
	 }
	 if (nbt.getBoolean ("static"))return;
	 if (nbt.getDouble ("buy")!=null||nbt.getDouble ("sell")!=null){
	   ClickType type = e.getClick ();
	   if ((type.equals (ClickType.RIGHT) || type.equals (ClickType.SHIFT_RIGHT) )&& nbt.getDouble ("buy") != null) {

		try {
		  if (Economy.getMoney (p.getName ()) < nbt.getDouble ("buy")) {
		    p.sendMessage ("§4§l<!>§c You do not have enough money to purchase this.");
		    return;
		  }
		  Economy.subtract (p.getName (), nbt.getDouble ("buy"));
		  p.sendMessage ("§6§l<!>§e You bought §f§l" + item.getAmount () + "§ex §f§l" + item.getType ().name ().replace ('_', ' ') + "§e for §f§l" + formatMoney (nbt.getDouble ("buy")) + "§e!");
		  if (nbt.getString ("name")!=null) {
		    for (int i = 0; i < item.getAmount (); i++) {
			 p.getInventory ().addItem (new ItemStackBuilder (new ItemStack (item.getType (), 1, item.getDurability ())).getDisplayName (nbt.getString ("name")).build ());
		    }
		  }
		} catch (UserDoesNotExistException | NoLoanPermittedException e1) {
		  e1.printStackTrace ();
		}
	   }else if (type.equals (ClickType.LEFT) && nbt.getDouble ("sell") != null){
		if (nbt.getDouble ("sell")==0.0){
		  p.sendMessage ("§4§l<!>§c You're not allowed to sell this item.");
		  return;
		}
		double money = 0.0; int amount = 0;
		try {
		  for (ItemStack i : p.getInventory ().getContents ()) {
		    if (i != null && i.getType ().equals (item.getType ())) {
			 p.getInventory ().removeItem (i);
			 amount = amount + i.getAmount ();
			 money = money + i.getAmount () *(nbt.getDouble ("sell")/item.getAmount ());
		    }
		  }
		  if (amount==0){
		    p.sendMessage ("§4§l<!>§c You do have enough §f§l" + item.getType ().name ().replace ('_', ' ') + "§c items to sell.");
		  }else {
		    Economy.add (p.getName (), new BigDecimal (money));
		    p.sendMessage ("§6§l<!>§e You sold §f§l" + amount + "§ex §f§l" + item.getType ().name ().replace ('_', ' ') + "§e for §f§l" + formatMoney (money) + "§e!");
		  }
		  return;
		} catch (UserDoesNotExistException | NoLoanPermittedException e1) {
		  e1.printStackTrace ();
		}
		p.sendMessage ("§4§l<!>§c You do have enough §f§l" + item.getType ().name ().replace ('_', ' ') + "§c items to sell.");
	   }
	 }
    }
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onWorldChange(PlayerChangedWorldEvent e){
    if (e.getPlayer ().getWorld ().getName ().equalsIgnoreCase ("faction_the_end")){
	 e.getPlayer ().teleport (Bukkit.getWorld ("End").getSpawnLocation ());
	 e.getPlayer ().sendMessage ("§4§l<?>§c You tried to connect to the old end, sent you to end v2! :D");
    }
  }

  /**
   * @author Zombie
   *
   * @param e PlayerInteractEvent, handles player's notes and spawn utils.
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onInteract(PlayerInteractEvent e){
    Player p = e.getPlayer ();

    if (e.getAction ().equals (Action.RIGHT_CLICK_BLOCK) &&
	   BoardColl.get ().getFactionAt (PS.valueOf (p)).equals (FactionColl.get ().getSafezone ())
	   && e.getClickedBlock ()!=null){
	 switch (e.getClickedBlock ().getType ()){
	   case ENDER_CHEST:
		((CraftPlayer) e.getPlayer ()).getHandle ().openContainer (((CraftPlayer) e.getPlayer ()).getHandle ().getEnderChest ());
		break;
	   case ANVIL:
		e.getPlayer ().openInventory (Bukkit.createInventory (e.getPlayer (), InventoryType.ANVIL));
		break;
	   case ENCHANTMENT_TABLE:
		e.getPlayer ().openInventory (Bukkit.createInventory (e.getPlayer (), InventoryType.ENCHANTING));
		break;
	 }
    }

    if (e.getAction ().equals (Action.RIGHT_CLICK_AIR) || e.getAction ().equals (Action.RIGHT_CLICK_BLOCK)) {
	 ItemStack item = p.getItemInHand ();
	 if (item == null) return;
	 if (item.getType ().equals (Material.PAPER) && item.getEnchantments ().containsKey (Enchantment.LUCK)) {
	   NBTItem nbt = new NBTItem (item);
	   //<-- MONEY NOTE -->
	   if (item.getItemMeta ().getDisplayName ().equalsIgnoreCase (FactionItems.getMoneyNote (0.0).getItemMeta ().getDisplayName ())) {
		try {
		  if (item.getAmount ()-1==0){
		    p.getInventory ().removeItem (item);
		  }else {
		    item.setAmount (item.getAmount ()-1);
		  }
		  p.sendMessage ("§2§l<!>§a You redeems a money note for §f§l" + formatMoney (nbt.getDouble ("money")) + "§a!");
		  Economy.add (p.getName (), new BigDecimal (nbt.getDouble ("money")));
		  update ();
		} catch (UserDoesNotExistException | NoLoanPermittedException e1) {
		  e1.printStackTrace ();
		}
	   }
	   if (item.getItemMeta ().getDisplayName ().equalsIgnoreCase (FactionItems.getMCMMONote (0, "swords").getItemMeta ().getDisplayName ())){
		ExperienceAPI.addXP (p, nbt.getString ("skill"), nbt.getInteger ("mcmmo"));
	   }
	   if (item.getItemMeta ().getDisplayName ().equalsIgnoreCase (FactionItems.getKitNote (new StarterKit ()).getItemMeta ().getDisplayName ())){
		FactionKit kit = Core.get ().getKitManager ().valueOf (nbt.getString ("kit"));
		Core.get ().getKitManager ().giveKit (p, kit);
	   }
	 }
    }
  }

  /**
   * @author Zombie
   *
   * @param e KitUseEvent, handles the kit coolDown and giving the actual kit.
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onKitUseEvent(KitUseEvent e){
    Player p = e.getPlayer ();
    FactionKit kit = e.getKit ();
    KitManager manager = Core.get ().getKitManager ();

    if (p.getItemInHand ()!=null){
	 if (p.getItemInHand ().getAmount ()==1){
	   p.setItemInHand (new ItemStack (Material.AIR));
	 }else {
	   p.getItemInHand ().setAmount (p.getItemInHand ().getAmount ()-1);
	 }
    }

    if (manager.isDefined (p, kit) && manager.isCoolDownOver (p, kit)){
	 p.sendMessage ("§4§l<!>§c You have " + Core.timerTans ((int) ((manager.getCoolDown (p, kit) - System.currentTimeMillis ())/1000)) + " before you can use this kit again!");
	 e.setCancelled (true);
    }else {
	 manager.giveKit (p, kit);
    }
  }

  /**
   * @author Zombie
   *
   * @param e EventFactionsCreate, broadcasts player's new faction
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onFactionCreate(EventFactionsCreate e){
    Bukkit.broadcastMessage ("§2§l<!>§a " + e.getSender ().getName () + " has created faction §f§l" + e.getFactionName () + "§a!");
  }

  /**
   * @author Zombie
   *
   * @param e PlayerDeathEvent, broadcasts player's death messages
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onDeath(PlayerDeathEvent e){
    e.setDeathMessage ("§6§l<!>§e " + e.getDeathMessage ());
  }

  /**
   * @author Zombie
   *
   * @param e EntitySpawnEvent, prevents monsters from spawning in warzone
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onSpawn(EntitySpawnEvent e){
    if (e.getEntity () instanceof Monster && BoardColl.get ().getFactionAt (PS.valueOf (e.getLocation ())).equals (FactionColl.get ().getWarzone ())){
	 e.setCancelled (true);
    }
  }

  /**
   * @author Zombie
   *
   * @param e BlockFromToEvent, mimics 1.7 sponge physics
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onSponge(BlockFromToEvent e){
    for (Block b : GameAPI.blocksBetween (e.getBlock ().getLocation (), 3)){
	 e.setCancelled (b.getType ().equals (Material.SPONGE));
    }
  }

  @EventHandler
  public void onDisable(PluginDisableEvent e){
    if (e.getPlugin ().equals (GameAPI.get ())){
	 for (Player p : Bukkit.getOnlinePlayers ()){
	   p.setWalkSpeed (0); p.setFlySpeed (0);
	   GameAPI.sendTitle (p, "§4§k*§c§lRESTARTING§4§k*", "§cPlease wait a moment before doing anything.");
	 }
    }
  }

  @EventHandler
  public void onEnable(PluginEnableEvent e) {
    if (e.getPlugin ().equals (GameAPI.get ())) {
	 for (Player p : Bukkit.getOnlinePlayers ()) {
	   Bukkit.getPluginManager ().callEvent (new PlayerLoginEvent (p, ""));
	   Bukkit.getPluginManager ().callEvent (new PlayerJoinEvent (p, ""));

	   p.setWalkSpeed ((float) 0.2);
	   p.setFlySpeed ((float) 0.2);
	   GameAPI.sendTitle (p, "§2§k*§a§lRESTART COMPLETE§2§k*", "§aResume playing as normal with updates :D");
	   GameAPI.sendTabBar (p, "§f§k§llll §9§lEPICRAFT §f§k§llll\n", "\n§f§lYou are connected to §9§l§n" + Bukkit.getServerName ());
	 }
    }
  }

}
