package me.zombiegalaxydev.faction.utils;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.massivecore.ps.PS;
import me.zombiegalaxydev.faction.Core;
import me.zombiegalaxydev.faction.enchants.CustomEnchant;
import me.zombieghostdev.gameapi.utils.ItemStackBuilder;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;
import org.bukkit.*;
import org.bukkit.block.BlockState;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CrateManager implements Listener{

  private List<ItemStack> contents = new ArrayList<> ();
  private ArmorStand stand;

  public CrateManager () {
    contents.add (new ItemStackBuilder (Material.DIAMOND_BOOTS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 1).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_BOOTS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_BOOTS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_BOOTS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_BOOTS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 1).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_BOOTS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 2).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_BOOTS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_BOOTS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_BOOTS).getUnsafeEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 6).getEnchantment (Enchantment.DURABILITY, 3).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 1).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 1).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 2).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getUnsafeEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 6).getEnchantment (Enchantment.DURABILITY, 3).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 1).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 1).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 2).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getUnsafeEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 6).getEnchantment (Enchantment.DURABILITY, 3).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_HELMET).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 1).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_HELMET).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_HELMET).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_HELMET).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).build ());    contents.add (new ItemStackBuilder (Material.DIAMOND_HELMET).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 1).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_HELMET).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 2).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_HELMET).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 3).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_HELMET).getEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 4).getEnchantment (Enchantment.DURABILITY, 2).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_HELMET).getUnsafeEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 6).getEnchantment (Enchantment.DURABILITY, 3).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_PICKAXE).getUnsafeEnchantment (Enchantment.LOOT_BONUS_BLOCKS, 5).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_PICKAXE).getUnsafeEnchantment (Enchantment.LOOT_BONUS_BLOCKS, 7).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_PICKAXE).getUnsafeEnchantment (Enchantment.LOOT_BONUS_BLOCKS, 5).getEnchantment (Enchantment.DIG_SPEED, 3).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_PICKAXE).getUnsafeEnchantment (Enchantment.LOOT_BONUS_BLOCKS, 7).getUnsafeEnchantment (Enchantment.DIG_SPEED, 5).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_AXE).getUnsafeEnchantment (Enchantment.LOOT_BONUS_BLOCKS, 5).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_AXE).getUnsafeEnchantment (Enchantment.LOOT_BONUS_BLOCKS, 7).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_AXE).getUnsafeEnchantment (Enchantment.LOOT_BONUS_BLOCKS, 5).getEnchantment (Enchantment.DIG_SPEED, 3).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_AXE).getUnsafeEnchantment (Enchantment.LOOT_BONUS_BLOCKS, 7).getUnsafeEnchantment (Enchantment.DIG_SPEED, 5).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_SWORD).getEnchantment (Enchantment.DAMAGE_ALL, 4).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_SWORD).getEnchantment (Enchantment.DAMAGE_ALL, 5).build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_AXE).getUnsafeEnchantment (Enchantment.DAMAGE_ALL, 10).getDisplayName ("§6§kl§5§lJesus Axe§6§kl").build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_SWORD).getUnsafeEnchantment (Enchantment.DAMAGE_ALL, 10).getDisplayName ("§6§kl§5§lJesus Sword§6§kl").build ());
    contents.add (new ItemStackBuilder (Material.BOW).getUnsafeEnchantment (Enchantment.ARROW_DAMAGE, 10).getEnchantment (Enchantment.ARROW_INFINITE, 1).getDisplayName ("§6§kl§5§lJesus Bow§6§kl").build ());
    contents.add (Core.get ().getCustomEnchantManager ().enchantItem (new ItemStack (Material.GOLD_SWORD), CustomEnchant.LIFESTEAL, 5));
    contents.add (Core.get ().getCustomEnchantManager ().enchantItem (new ItemStack (Material.DIAMOND_SWORD), CustomEnchant.DEATHBRINGER, 2));
    contents.add (new ItemStackBuilder (Material.DIAMOND_HELMET).getUnsafeEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 10).getUnsafeEnchantment (Enchantment.DURABILITY, 5).getDisplayName ("§f§lPhoenix Helmet").build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_CHESTPLATE).getUnsafeEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 10).getUnsafeEnchantment (Enchantment.DURABILITY, 5).getDisplayName ("§f§lPhoenix Chestplate").build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_LEGGINGS).getUnsafeEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 10).getUnsafeEnchantment (Enchantment.DURABILITY, 5).getDisplayName ("§f§lPhoenix Leggings").build ());
    contents.add (new ItemStackBuilder (Material.DIAMOND_BOOTS).getUnsafeEnchantment (Enchantment.PROTECTION_ENVIRONMENTAL, 10).getUnsafeEnchantment (Enchantment.DURABILITY, 5).getDisplayName ("§f§lPhoenix Boots").build ());
    contents.add (new ItemStack (Material.GOLD_INGOT, 16));
    contents.add (new ItemStack (Material.MONSTER_EGG, 1, (short) 50));
    contents.add (new ItemStack (Material.GOLD_INGOT, 64));
    contents.add (new ItemStack (Material.COAL, 16));
    contents.add (new ItemStack (Material.COAL, 64));
    contents.add (new ItemStack (Material.TNT, 64));
    contents.add (new ItemStack (Material.IRON_INGOT, 16));
    contents.add (new ItemStack (Material.IRON_INGOT, 64));
    contents.add (new ItemStack (Material.DIAMOND, 16));
    contents.add (new ItemStack (Material.DIAMOND, 64));
    contents.add (new ItemStack (Material.OBSIDIAN, 32));
    contents.add (new ItemStack (Material.OBSIDIAN, 64));
    contents.add (new ItemStack (Material.BEDROCK, 2));
    contents.add (new ItemStack (Material.GOLDEN_APPLE, 1));
    contents.add (new ItemStack (Material.GOLDEN_APPLE, 4));
    contents.add (new ItemStack (Material.GOLDEN_APPLE, 16));
    contents.add (new ItemStack (Material.GOLDEN_APPLE, 1, (short) 1));
    contents.add (new ItemStack (Material.GOLDEN_APPLE, 4, (short) 1));
    contents.add (new ItemStack (Material.GOLDEN_APPLE, 8, (short) 1));
    contents.add (new ItemStackBuilder (Material.SLIME_BALL).getUnsafeEnchantment (Enchantment.KNOCKBACK, 10).getDisplayName ("§fSuper Bouncy Ball").build ());
    contents.add (FactionItems.getMoneyNote (5000));
    contents.add (FactionItems.getMoneyNote (10000));
    contents.add (FactionItems.getMoneyNote (25000));
    contents.add (FactionItems.getMoneyNote (50000));
    contents.add (FactionItems.getMoneyNote (75000));
    contents.add (FactionItems.getMoneyNote (100000));
    contents.add (FactionItems.getMoneyNote (250000));

    stand = getCrateDisplay ();
    new BukkitRunnable () {
	 int ticks = 600*3;
	 @Override
	 public void run () {
	   for (Entity entity : stand.getNearbyEntities (3, 3, 3)) {
		if ((entity instanceof ArmorStand) && entity.getUniqueId () != stand.getUniqueId ()) {
		  entity.remove ();
		}
	   }
	   stand.setCustomName ("§4§k|§cCrate drop in §f§l" + Core.timerTans ((ticks--)) + "§c!§4§k|");
	   if (ticks==0){
		Bukkit.broadcastMessage ("§8§m-----------------------------§r \n \n \n §4§l<!!!> §c§lAlert§c Crate drop at spawn! §4§l<!!!>\n \n \n§8§m-----------------------------§r");
		new BukkitRunnable () {
		  @Override
		  public void run () {
		    List<PS> pss = new ArrayList<> ();
		    pss.addAll (BoardColl.get ().getChunks (FactionColl.get ().getWarzone ()));
		    for (int i = 0; i < 50; i++) {
			 Location loc = pss.get (new Random ().nextInt (pss.size ())).getChunk ().asBukkitChunk ().getBlock (new Random ().nextInt (15)-new Random ().nextInt (15), 250, new Random ().nextInt (15)-new Random ().nextInt (15)).getLocation ();
			 loc.getWorld ().spawnFallingBlock (loc, Material.CHEST, (byte) 1).setDropItem (false);
		    }
		  }
		}.runTask (Core.get ());
	   }
	   if (ticks<=0){
		ticks = 600*3;
	   }
	 }
    }.runTaskTimerAsynchronously (Core.get (), 0L, 20L);
    new BukkitRunnable () {
	 @Override
	 public void run () {
	   for (PS ps : BoardColl.get ().getChunks (FactionColl.get ().getWarzone ())) {
		for (BlockState b : ps.asBukkitChunk ().getTileEntities ()) {
		  if (!b.getType ().equals (Material.CHEST)) continue;
		  for (Player p : b.getWorld ().getPlayers ()) {
		    Location location = b.getLocation ().add (.5, 1.75, .5);
		    ((CraftPlayer) p).getHandle ().playerConnection.sendPacket ( new PacketPlayOutWorldParticles (EnumParticle.FIREWORKS_SPARK, true, (float) location.getX (), (float) location.getY (), (float) location.getZ (), (float) .5, 0, (float) .5, 0, 5, null));
		  }
		}
	   }
	 }
    }.runTaskTimer (Core.get (), 0L, 10L);
    new BukkitRunnable () {
	 @Override
	 public void run () {
	   for (PS ps : BoardColl.get ().getChunks (FactionColl.get ().getWarzone ())) {
		for (BlockState b : ps.asBukkitChunk ().getTileEntities ()) {
		  if (!b.getType ().equals (Material.CHEST)) continue;
		  b.getBlock ().breakNaturally (new ItemStack (Material.AIR));
		}
	   }
	 }
    }.runTaskLater (Core.get (), 20*120);
  }

  public ArmorStand getCrateDisplay(){
    ArmorStand stand = Bukkit.getWorld ("faction").spawn (new Location (
	   Bukkit.getWorld ("faction"), 8.5, 85, 157.5), ArmorStand.class);
    stand.setMarker (true);
    stand.setVisible (false);
    stand.setSmall (true);
    stand.setGravity (false);
    stand.setCustomNameVisible (true);

    return stand;
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onInteract(PlayerInteractEvent e) {
    if (e.getClickedBlock ()==null)return;
    for (PS ps : BoardColl.get ().getChunks (FactionColl.get ().getWarzone ())) {
	 for (BlockState b : ps.asBukkitChunk ().getTileEntities ()) {
	   if (b.equals (e.getClickedBlock ().getState ())) {
		e.getClickedBlock ().setType (Material.AIR);
		for (int i = 0; i < new Random ().nextInt (3); i++) {
		  e.getClickedBlock ().getWorld ().dropItem (e.getClickedBlock ().getLocation (), contents.get (new Random ().nextInt (contents.size ())));
		}
		Location location = e.getClickedBlock ().getLocation ();
		((CraftPlayer) e.getPlayer ()).getHandle ().playerConnection.sendPacket (new PacketPlayOutWorldParticles (EnumParticle.LAVA, true, (float) location.getX (), (float) location.getY (), (float) location.getZ (), (float) .5, 0, (float) .5, 0, 5, null));
		e.getPlayer ().playSound (location, Sound.IRONGOLEM_HIT, 1, 1);
		e.setCancelled (true);
		return;
	   }
	 }
    }
  }

}
