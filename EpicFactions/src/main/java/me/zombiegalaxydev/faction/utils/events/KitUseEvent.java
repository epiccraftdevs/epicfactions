package me.zombiegalaxydev.faction.utils.events;
import me.zombiegalaxydev.faction.kits.FactionKit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class KitUseEvent extends Event implements Cancellable {

  private static HandlerList handlerList = new HandlerList ();

  private FactionKit kit;
  private Player player;

  public KitUseEvent (FactionKit kit, Player player) {
    this.kit = kit;
    this.player = player;
  }

  public FactionKit getKit () {
    return kit;
  }

  public Player getPlayer () {
    return player;
  }

  public static HandlerList getHandlerList () {
    return handlerList;
  }

  @Override
  public boolean isCancelled () {
    return false;
  }

  @Override
  public void setCancelled (boolean b) {
  }

  @Override
  public HandlerList getHandlers () {
    return handlerList;
  }
}
