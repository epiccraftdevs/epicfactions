package me.zombiegalaxydev.faction.utils;
import me.zombieghostdev.gameapi.utils.ItemStackBuilder;
import me.zombieghostdev.gameapi.utils.itemnbtapi.NBTItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ShopManager {

  private List<Inventory> shops = new ArrayList<> ();

  boolean isShop (Inventory inventory){
    for (Inventory shop : shops){
	 if (shop.getTitle ().equalsIgnoreCase (inventory.getTitle ())){
	   return true;
	 }
    }
    return false;
  }

  Inventory getShop (String string){
    switch (string) {
	 case "build":
	   return buildingShop ();
	 case "spawner":
	   return spawnerShop ();
	 case "food":
	   return foodShop ();
	 case "material":
	   return materialShop ();
	 case "farm":
	   return farmShop ();
	 case "deco":
	   return decoShop ();
	 case "redstone":
	   return redstoneShop ();
    }
    return shopMenu ();
  }

  public ShopManager () {
    shops.add (buildingShop ());
    shops.add (spawnerShop ());
    shops.add (foodShop ());
    shops.add (materialShop ());
    shops.add (farmShop ());
    shops.add (decoShop ());
    shops.add (redstoneShop ());
  }

  public Inventory shopMenu(){
    Inventory inv = Bukkit.createInventory (null, 18, "§0§lShop Menu");

    for (int i = 0; i < inv.getSize (); i++) {
	 if (i<=8){
	   ItemStack item = new ItemStack (Material.STAINED_GLASS_PANE, 1, (short) 15);
	   ItemMeta meta = item.getItemMeta ();
	   meta.setDisplayName (" ");
	   item.setItemMeta (meta);

	   item.addUnsafeEnchantment (Enchantment.LUCK, 1);

	   NBTItem nbt = new NBTItem (item);
	   nbt.setInteger ("HideFlags", 32 + 1);
	   inv.setItem (i, nbt.getItem ());
	 }
    }

    {
	 ItemStack item = new ItemStack (Material.CHEST);
	 ItemMeta meta = item.getItemMeta ();
	 meta.setDisplayName ("§e§lShop Menu");
	 item.setItemMeta (meta);
	 inv.setItem (4, item);
    }

    {
	 ItemStack item = new ItemStack (Material.GRASS);
	 ItemMeta meta = item.getItemMeta ();
	 meta.setDisplayName ("§2§lBuilding Shop");
	 item.setItemMeta (meta);

	 NBTItem nbt = new NBTItem (item);
	 nbt.setString ("shop", "build");
	 inv.addItem (nbt.getItem ());
    }

    {
	 ItemStack item = new ItemStack (Material.MOB_SPAWNER);
	 ItemMeta meta = item.getItemMeta ();
	 meta.setDisplayName ("§2§lSpawner Shop");
	 item.setItemMeta (meta);

	 NBTItem nbt = new NBTItem (item);
	 nbt.setString ("shop", "spawner");
	 inv.addItem (nbt.getItem ());
    }
    inv.addItem (new ItemStackBuilder(Material.REDSTONE).getDisplayName ("§2§lRestone Shop").getNBTTag ("shop", "redstone").build ());
    inv.addItem (new ItemStackBuilder(Material.APPLE).getDisplayName ("§2§lFood Shop").getNBTTag ("shop", "food").build ());
    inv.addItem (new ItemStackBuilder(Material.IRON_INGOT).getDisplayName ("§2§lMaterial Shop").getNBTTag ("shop", "material").build ());
    inv.addItem (new ItemStackBuilder(Material.DIAMOND_HOE).getDisplayName ("§2§lFarm Shop").getNBTTag ("shop", "farm").build ());
    inv.addItem (new ItemStackBuilder(Material.STAINED_CLAY).getDisplayName ("§2§lDecoration Shop").getNBTTag ("shop", "deco").build ());
    return inv;
  }

  private Inventory buildingShop (){
    Inventory inv = Bukkit.createInventory (null, 45, "§2§lBuilding Shop");
    buildHeader (inv);
	 inv.addItem (buildProduct (Material.NETHER_BRICK, 16, 100.0, 200.0), buildProduct (Material.BOOKSHELF, 16, 750.0, 2500), buildProduct (Material.GLOWSTONE, 16, 250.0, 500.0),
		buildProduct (Material.LOG, 16, 250.0, 500.0),
		buildProduct (Material.QUARTZ_BLOCK, 16, 100.0, 200.0),
		buildProduct (Material.ICE, 16, 1750.0, 2500.0),
		buildProduct (Material.ENDER_STONE, 16, 125.0, 250.0),
		buildProduct (Material.SOUL_SAND, 16, 100.0, 200.0),
		buildProduct (Material.PACKED_ICE, 16, 2500.0, 5000.0),
		buildProduct (Material.HARD_CLAY, 16, 375.0, 750.0),
		buildProduct (Material.BRICK, 16, 375.0, 750.0),
		buildProduct (Material.COBBLESTONE, 16, 100.0, 200.0),
		buildProduct (Material.getMaterial (98), 16, 375.0, 750.0),
		buildProduct (Material.GRASS, 16, 100.0, 200.0),
		buildProduct (Material.SAND, 16, 100.0, 200.0),
		buildProduct (Material.OBSIDIAN, 16, 750.0, 2500.0),
		buildProduct (Material.SPONGE, 16, 750.0, 2500.0),
		buildProduct (Material.getMaterial (44), 16, 25.0, 50.0),
		buildProduct (Material.STONE, 16, 150.0, 300.0),
		buildProduct (Material.BREWING_STAND_ITEM, 1, 40.50, 81.25),
		buildProduct (Material.GLASS, 16, 100.0, 200.0),
		buildProduct (Material.DIRT, 16, 100.0, 200.0),
		buildProduct (Material.SANDSTONE, 16, 50.0, 100.0));
    return inv;
  }

  private Inventory spawnerShop (){
    Inventory inv = Bukkit.createInventory (null, 27, "§2§lSpawner Shop");
    buildHeader (inv);
    inv.addItem (buildProduct (Material.MOB_SPAWNER, "§e§lPigmen§r Spawner", 1, 0.0, 450000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lBlaze§r Spawner", 1, 0.0, 300000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lPig§r Spawner", 1, 0.0, 25000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lCreeper§r Spawner", 1, 0.0, 300000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lEnderman§r Spawner", 1, 0.0, 200000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lSlime§r Spawner", 1, 0.0, 200000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lSpider§r Spawner", 1, 0.0, 75000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lCave_Spider§r Spawner", 1, 0.0, 75000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lSkeleton§r Spawner", 1, 0.0, 75000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lZombie§r Spawner", 1, 0.0, 75000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lChicken§r Spawner", 1, 0.0, 25000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lWolf§r Spawner", 1, 0.0, 25000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lCow§r Spawner", 1, 0.0, 75000),
	   buildProduct (Material.MOB_SPAWNER, "§e§lIron_Golem§r Spawner", 1, 0.0, 1500000));
    return inv;
  }

  private Inventory foodShop (){
    Inventory inv = Bukkit.createInventory (null, 45, "§2§lFood Shop");
    buildHeader (inv);
    inv.addItem (
	   buildProduct (Material.GOLDEN_APPLE, 1, 0.0, 1000.0),
	   buildProduct (new ItemStack (Material.GOLDEN_APPLE, 1, (short) 1), 0.0, 12000.0),
	   buildProduct (Material.APPLE, 16, 50.0, 100.0),
	   buildProduct (Material.CARROT_ITEM, 16, 25.0, 50.0),
	   buildProduct (Material.COOKED_BEEF, 16, 150.0, 300.0),
	   buildProduct (Material.COOKIE, 16, 125.0, 250.0),
	   buildProduct (Material.BREAD, 16, 25.0, 50.0),
	   buildProduct (Material.MELON, 16, 60.0, 120.0),
	   buildProduct (Material.BAKED_POTATO, 16, 100.0, 200.0),
	   buildProduct (Material.CAKE, 1, 50.0, 100.0));
    return inv;
  }

  private Inventory materialShop (){
    Inventory inv = Bukkit.createInventory (null, 63, "§2§lMaterial Shop");
    buildHeader (inv);
    inv.addItem (
	   buildProduct (Material.COAL, 64, 600.0, 1200.0),
	   buildProduct (Material.COAL, 16, 150.0, 300.0),
	   buildProduct (new ItemStack (Material.INK_SACK, 64, (short) 4), 600.0, 1200.0),
	   buildProduct (new ItemStack (Material.INK_SACK, 16, (short) 4), 150.0, 300.0),
	   buildProduct (Material.IRON_INGOT, 64, 1600.0, 3200.0),
	   buildProduct (Material.IRON_INGOT, 16, 400.0, 800.0),
	   buildProduct (Material.GOLD_INGOT, 64, 2400.0, 4800.0),
	   buildProduct (Material.GOLD_INGOT, 16, 600.0, 1200.0),
	   buildProduct (Material.DIAMOND, 64, 3200.0, 6400.0),
	   buildProduct (Material.DIAMOND, 16, 800.0, 1600.0),
	   buildProduct (Material.EMERALD, 64, 6400.0, 12800.0),
	   buildProduct (Material.EMERALD, 16, 1600.0, 3200.0),
	   buildProduct (Material.SULPHUR, 64, 400.0, 800.0),
	   buildProduct (Material.SULPHUR, 16, 100.0, 200.0),
	   buildProduct (Material.LEATHER, 64, 50.0, 100.0),
	   buildProduct (Material.LEATHER, 16, 12.5, 25.0),
	   buildProduct (Material.ENDER_PEARL, 64, 675.0, 1350.0),
	   buildProduct (Material.ENDER_PEARL, 16, 168.75, 337.5),
	   buildProduct (Material.ROTTEN_FLESH, 64, 60.0, 120.0),
	   buildProduct (Material.ROTTEN_FLESH, 16, 15.0, 30.0),
	   buildProduct (Material.COOKED_BEEF, 64, 150.0, 300.0),
	   buildProduct (Material.COOKED_BEEF, 16, 37.5, 75.0),

	   buildProduct (Material.BLAZE_ROD, 64, 2000.0, 4000.0),
	   buildProduct (Material.BLAZE_ROD, 16, 500.0, 1000.0),
	   buildProduct (Material.BONE, 64, 60.0, 120.0),
	   buildProduct (Material.BONE, 16, 15.0, 30.0),
	   buildProduct (Material.MELON, 64, 50, 100.0),
	   buildProduct (Material.MELON, 16, 12.5, 25.0),
	   buildProduct (new ItemStack (Material.INK_SACK, 64, (short) 3), 200.0, 400.0),
	   buildProduct (new ItemStack (Material.INK_SACK, 16, (short) 3), 50.0, 100.0),
	   buildProduct (Material.getMaterial (372), 64, 200.0, 400.0),
	   buildProduct (Material.getMaterial (372), 16, 50.0, 100.0),
	   buildProduct (Material.SUGAR_CANE, 64, 350.0, 700.0),
	   buildProduct (Material.SUGAR_CANE, 16, 87.5, 175.0),
	   buildProduct (Material.WHEAT, 64, 200.0, 400.0),
	   buildProduct (Material.WHEAT, 16, 50.0, 100.0),
	   buildProduct (Material.CACTUS, 64, 600.0, 1200.0),
	   buildProduct (Material.CACTUS, 16, 150.0, 300.0),
	   buildProduct (Material.PUMPKIN, 64, 700.0, 1400.0),
	   buildProduct (Material.PUMPKIN, 16, 175.0, 350.0),
	   buildProduct (Material.NAME_TAG, 1, 2500.0, 5000.0),
	   buildProduct (Material.VINE, 6, 125.0, 250.0),
	   buildProduct (Material.SPECKLED_MELON, 16, 110.0, 200.0),
	   buildProduct (Material.FLINT_AND_STEEL, 1, 125.0, 250.0),
	   buildProduct (Material.STICK, 16, 50.0, 100.0),
	   buildProduct (Material.SIGN, 8, 100.0, 200.0),
	   buildProduct (Material.BUCKET, 16, 250.0, 500.0),
	   buildProduct (Material.LAVA_BUCKET, 8, 200.0, 400.0),
	   buildProduct (Material.WATER_BUCKET, 8, 200.0, 400.0),
	   buildProduct (Material.ITEM_FRAME, 16, 50.0, 100.0));

    return inv;
  }

  private Inventory farmShop (){
    Inventory inv = Bukkit.createInventory (null, 27, "§2§lFarm Shop");
    buildHeader (inv);
    inv.addItem (
	   buildProduct (Material.MELON_SEEDS, 16, 100.0, 200.0),
	   buildProduct (Material.CACTUS, 16, 600.0, 1200.0),
	   buildProduct (Material.getMaterial (372), 16, 200.0, 400.0),
	   buildProduct (Material.SUGAR_CANE, 16, 350.0, 700.0),
	   buildProduct (Material.PUMPKIN_SEEDS, 16, 350.0, 700.0),
	   buildProduct (Material.ARROW, 16, 50.0, 100.0),
	   buildProduct (Material.WEB, 16, 125.0, 250.0),
	   buildProduct (Material.BLAZE_ROD, 16, 500.0, 1000.0),
	   buildProduct (Material.SULPHUR, 16, 250.0, 500.0),

	   buildProduct (Material.BONE, 16, 60.0, 120.0),
	   buildProduct (Material.GHAST_TEAR, 16, 100.0, 200.0),
	   buildProduct (Material.INK_SACK, 16, 50, 100.0),
	   buildProduct (Material.FERMENTED_SPIDER_EYE, 16, 100.0, 200.0),
	   buildProduct (Material.ENDER_PEARL, 16, 200.0, 400.0),
	   buildProduct (Material.SLIME_BALL, 16, 100.0, 200.0),
	   buildProduct (Material.MAGMA_CREAM, 16, 100.0, 200.0));
    return inv;
  }

  private Inventory decoShop (){
    Inventory inv = Bukkit.createInventory (null, 63, "§2§lDeco Shop");
    buildHeader (inv);
    for (int i = 0; i <= 15; i++) {
	 inv.addItem (buildProduct (new ItemStack (Material.WOOL, 64, (short) i), 250.0, 500.0));
    }
    for (int i = 0; i <= 15; i++) {
	 inv.addItem (buildProduct (new ItemStack (Material.STAINED_GLASS, 64, (short) i), 250.0, 500.0));
    }
    for (int i = 0; i <= 15; i++) {
	 inv.addItem (buildProduct (new ItemStack (Material.STAINED_CLAY, 64, (short) i), 250.0, 500.0));
    }
    return inv;
  }

  private Inventory redstoneShop (){
    Inventory inv = Bukkit.createInventory (null, 36, "§2§lRedstone Shop");
    buildHeader (inv);
    inv.addItem (buildProduct (Material.HOPPER, 1, 1000, 2000));
    inv.addItem (buildProduct (Material.REDSTONE, 16, 125, 350));
    inv.addItem (buildProduct (Material.REDSTONE, 64, 700, 1400));
    inv.addItem (buildProduct (Material.REDSTONE_TORCH_ON, 16, 240, 480));
    inv.addItem (buildProduct (Material.REDSTONE_TORCH_ON, 64, 960, 1920));
    inv.addItem (buildProduct (Material.getMaterial (356), 1, 125, 350));
    inv.addItem (buildProduct (Material.getMaterial (356), 16, 700, 1400));
    inv.addItem (buildProduct (Material.DISPENSER, 16, 4000, 8000));
    inv.addItem (buildProduct (Material.DROPPER, 16, 3000, 6000));
    inv.addItem (buildProduct (Material.TNT, 16, 1250, 2500));
    inv.addItem (buildProduct (Material.TRIPWIRE_HOOK, 16, 125, 350));
    inv.addItem (buildProduct (Material.PISTON_BASE, 1, 100, 200));
    inv.addItem (buildProduct (Material.PISTON_BASE, 16, 1600, 3200));
    inv.addItem (buildProduct (Material.PISTON_STICKY_BASE, 1, 300, 500));
    inv.addItem (buildProduct (Material.PISTON_STICKY_BASE, 16, 2100, 3700));
    inv.addItem (buildProduct (Material.REDSTONE_COMPARATOR, 1, 625, 850));
    inv.addItem (buildProduct (Material.REDSTONE_COMPARATOR, 16, 1200, 1900));
    inv.addItem (buildProduct (Material.CHEST, 16, 125, 250));
    inv.addItem (buildProduct (Material.TRAPPED_CHEST, 16, 250, 500));
    inv.addItem (buildProduct (Material.LEVER, 16, 75, 150));
    inv.addItem (buildProduct (Material.STONE_BUTTON, 16, 50, 100));
    inv.addItem (buildProduct (Material.WOOD_BUTTON, 16, 50, 100));
    inv.addItem (buildProduct (new ItemStack (Material.MONSTER_EGG, 1, (short) 50), 100000, 200000));
    return inv;
  }


  private ItemStack buildProduct (Material material, int amount, double sell, double buy){
    return buildProduct (material, (short) 0, amount, sell, buy);
  }

  private ItemStack buildProduct (Material material, short data, int amount, double sell, double buy){
    NBTItem nbt = new NBTItem (new ItemStack (material, amount));
    nbt.setDouble ("sell", sell);
    nbt.setDouble ("buy", buy);

    ItemStack item = nbt.getItem ();
    ItemMeta meta = item.getItemMeta ();
    meta.setDisplayName ("§e§l" + item.getType ().name ().replace ('_', ' ') + " §8(§7Click Item§8)");
    List<String> lore = new ArrayList<> ();
    lore.add ("§7§l§m----------------------");
    if (buy>0.0){
	 lore.add ("§9Right click to buy this for§7:§e§l " + CoreManager.formatMoney (buy));
    }else {
	 lore.add ("§cYou can not buy this item.");
    }
    lore.add (" ");
    if (sell>0.0){
	 lore.add ("§aLeft click to sell this for§7:§e§l " + CoreManager.formatMoney (sell) + " §aeach");
    }else {
	 lore.add ("§cYou can not sell this item.");
    }
    lore.add ("§7§l§m---------------------- ");
    meta.setLore (lore);
    item.setItemMeta (meta);
    item.setDurability (data);
    return item;
  }

  public ItemStack buildProduct(Material material, String name, short data, int amount, double sell, double buy){
    ItemStack item = buildProduct (material, data, amount, sell, buy);
    ItemMeta meta = item.getItemMeta ();
    meta.setDisplayName (name);
    item.setItemMeta (meta);
    return item;
  }

  private ItemStack buildProduct (ItemStack item, double sell, double buy){
    return buildProduct (item.getType (), item.getDurability (), item.getAmount (), sell, buy);
  }

  private ItemStack buildProduct (Material material, String name, int amount, double sell, double buy){
    return new ItemStackBuilder (buildProduct (material, amount, sell, buy)).getDisplayName (name).getNBTTag ("name", name).build ();
  }

  private void buildHeader (Inventory inv){
    for (int i = 0; i < inv.getSize (); i++) {
	 if (i<=8){
	   inv.setItem (i, new ItemStackBuilder (new ItemStack (Material.STAINED_GLASS_PANE, 1, (short) 15)).getDisplayName (" ").getUnsafeEnchantment (Enchantment.LUCK, 1).getNBTTag ("HideFlags", 32 + 1).getNBTTag ("static", true).build ());
	 }
    }
    inv.setItem (4, new ItemStackBuilder (Material.CHEST).getDisplayName ("§e§lShop Menu").getLore ("§6Click to go back").getNBTTag ("static", true).build ());
  }
}
