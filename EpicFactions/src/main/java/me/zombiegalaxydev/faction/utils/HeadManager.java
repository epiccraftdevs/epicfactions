package me.zombiegalaxydev.faction.utils;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;
import me.zombiegalaxydev.faction.Core;
import me.zombieghostdev.gameapi.utils.itemnbtapi.NBTItem;
import net.ess3.api.Economy;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class HeadManager implements Listener, CommandExecutor{
  
  private BukkitRunnable dropSkull(final org.bukkit.entity.Entity entity){
    return new BukkitRunnable () {
	 @Override
	 public void run () {
	   if (entity.isDead ()) {
		ItemStack skull = new ItemStack (Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal ());
		SkullMeta meta = (SkullMeta) skull.getItemMeta ();
		double value = 0;
		if (! (entity instanceof Player)) {
		  switch (entity.getType ()) {
		    case CREEPER:
			 meta.setOwner ("MHF_" + entity.toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 30;
			 break;
		    case SKELETON:
			 meta.setOwner ("MHF_" + entity.getType ().toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 7.5;
			 break;
		    case SPIDER:
			 meta.setOwner ("MHF_" + entity.getType ().toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 2.5;
			 break;
		    case ZOMBIE:
			 meta.setOwner ("MHF_" + entity.getType ().toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 7.5;
			 break;
		    case SLIME:
			 meta.setOwner ("MHF_" + entity.getType ().toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 20;
			 break;
		    case PIG_ZOMBIE:
			 meta.setOwner ("MHF_PigZombie");
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 40;
			 break;
		    case ENDERMAN:
			 meta.setOwner ("MHF_" + entity.getType ().toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 20;
			 break;
		    case CAVE_SPIDER:
			 meta.setOwner ("MHF_CaveSpider");
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 2.5;
			 break;
		    case BLAZE:
			 meta.setOwner ("MHF_" + entity.getType ().toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 30;
			 break;
		    case PIG:
			 meta.setOwner ("MHF_" + entity.getType ().toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 2.5;
			 break;
		    case SHEEP:
			 meta.setOwner ("MHF_" + entity.getType ().toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 2.5;
			 break;
		    case COW:
			 meta.setOwner ("MHF_" + entity.getType ().toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 7.5;
			 break;
		    case CHICKEN:
			 meta.setOwner ("MHF_" + entity.getType ().toString ());
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 2.5;
			 break;
		    case IRON_GOLEM:
			 meta.setOwner ("MHF_Golem");
			 meta.setDisplayName (entity.getType ().toString ());
			 value = 150;
			 break;
		  }
		}

		if (meta.getDisplayName () != null) {
		  meta.setDisplayName ("" + ChatColor.GREEN + ChatColor.BOLD + meta.getDisplayName ());
		  List<String> lore = new ArrayList<> ();
		  lore.add ("§fValue§7:§a " + CoreManager.formatMoney (value));
		  lore.add ("§7/sellheads §8sell heads");
		  meta.setLore (lore);
		  skull.setItemMeta (meta);
		  NBTItem nbt = new NBTItem (skull);
		  nbt.setDouble ("Value", value);
		  entity.getWorld ().dropItem (entity.getLocation (), nbt.getItem ());
		}
	   }
	 }
    };
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onEntityDamage(final EntityDamageEvent e) {
    dropSkull (e.getEntity ()).runTaskLater (Core.get (), 0L);
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onCloseInv(InventoryCloseEvent e){
    Inventory inv = e.getInventory ();
    double money = 0.0;
    if (inv.getTitle ().equalsIgnoreCase ("§0Sell Heads")) {
	 for (ItemStack item : inv.getContents ()) {
	   if (item == null) continue;
	   if (item.getType ().equals (Material.SKULL_ITEM)) {
		NBTItem nbt = new NBTItem (item);
		if (nbt.getDouble ("Value") != null) {
		  money = money + nbt.getDouble ("Value") * item.getAmount ();
		} else {
		  e.getPlayer ().getInventory ().addItem (item);
		}
	   }else {
		e.getPlayer ().getInventory ().addItem (item);
	   }
	 }
	 if (money>0) {
	   try {
		Economy.add (e.getPlayer ().getName (), money);
	   } catch (UserDoesNotExistException | NoLoanPermittedException e1) {
		e1.printStackTrace ();
	   } finally {
		((Player) e.getPlayer ()).playSound (e.getPlayer ().getEyeLocation (), Sound.LEVEL_UP, 1, 1);
		e.getPlayer ().sendMessage ("§2§l<!>§a You gained " + CoreManager.formatMoney (money) + " from selling heads!");
	   }
	 }
    }
  }

  @Override
  public boolean onCommand (CommandSender commandSender, Command command, String s, String[] strings) {
    if (command.getName ().equalsIgnoreCase ("sellheads")){
	 if (! (commandSender instanceof Player)){
	   return false;
	 }
	 Player p = (Player) commandSender;
	 Inventory inv = Bukkit.createInventory (null, 54, "§0Sell Heads");
	 p.openInventory (inv);
	 return true;
    }
    return false;
  }
}
