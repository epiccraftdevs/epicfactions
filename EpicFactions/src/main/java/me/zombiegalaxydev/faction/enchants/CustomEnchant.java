package me.zombiegalaxydev.faction.enchants;
public enum CustomEnchant {
  ENDURE(3, CustomEnchantType.ARMOR),
  ENLIGHTEN(3, CustomEnchantType.ARMOR),
  EVASION(1, CustomEnchantType.ARMOR),
  MOLTEN(3, CustomEnchantType.ARMOR),
  OBSIDIAN(10, CustomEnchantType.ARMOR),
  SELFDESTRUCT(3, CustomEnchantType.ARMOR),
  SHIELD(10, CustomEnchantType.ARMOR),
  TANK(10, CustomEnchantType.ARMOR),

  BLIND(3, CustomEnchantType.GLOBAL),
  CHARGE(3, CustomEnchantType.GLOBAL),
  CRIPPLE(3, CustomEnchantType.GLOBAL),
  DEATHBRINGER(5, CustomEnchantType.GLOBAL),
  FAMINE(1, CustomEnchantType.GLOBAL),
  GORE(0, CustomEnchantType.GLOBAL),
  GOO(10, CustomEnchantType.GLOBAL),
  LIFESTEAL(5, CustomEnchantType.GLOBAL),
  THUNDER(10, CustomEnchantType.GLOBAL),
  VENOM(5, CustomEnchantType.GLOBAL),
  WITHER(3, CustomEnchantType.GLOBAL);

  public enum CustomEnchantType{
    GLOBAL, ARMOR, BOOTS, HELMET, BOW, TOOL;
  }

  private int lvMax;
  private CustomEnchantType type;

  CustomEnchant (int lvMax, CustomEnchantType type) {
    this.lvMax = lvMax;
    this.type = type;
  }

  public int getLvMax () {
    return lvMax;
  }

  public CustomEnchantType getType () {
    return type;
  }
}
