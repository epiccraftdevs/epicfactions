package me.zombiegalaxydev.faction.enchants;
import me.zombieghostdev.gameapi.utils.ItemStackBuilder;
import me.zombieghostdev.gameapi.utils.itemnbtapi.NBTItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomEnchantManager {

  private List<CustomEnchant> customEnchants = new ArrayList<> ();

  public CustomEnchantManager () {
    //Armor Enchantments
    customEnchants.add (CustomEnchant.ENDURE);
    customEnchants.add (CustomEnchant.ENLIGHTEN);
    customEnchants.add (CustomEnchant.EVASION);
    customEnchants.add (CustomEnchant.MOLTEN);
    customEnchants.add (CustomEnchant.OBSIDIAN);
    customEnchants.add (CustomEnchant.SELFDESTRUCT);
    customEnchants.add (CustomEnchant.SHIELD);
    customEnchants.add (CustomEnchant.TANK);

    //Global Enchantments
    customEnchants.add (CustomEnchant.BLIND);
    customEnchants.add (CustomEnchant.CHARGE);
    customEnchants.add (CustomEnchant.CRIPPLE);
    customEnchants.add (CustomEnchant.DEATHBRINGER);
    customEnchants.add (CustomEnchant.FAMINE);
    customEnchants.add (CustomEnchant.GORE);
    customEnchants.add (CustomEnchant.LIFESTEAL);
    customEnchants.add (CustomEnchant.THUNDER);
    customEnchants.add (CustomEnchant.VENOM);
    customEnchants.add (CustomEnchant.WITHER);
  }

  public String levelFormat(int level){
    StringBuilder builder = new StringBuilder ();
    builder.append (" ");
    switch (level){
	 case 1:
	   builder.append ("I");
	   break;
	 case 2:
	   builder.append ("II");
	   break;
	 case 3:
	   builder.append ("III");
	   break;
	 case 4:
	   builder.append ("IV");
	   break;
	 case 5:
	   builder.append ("V");
	   break;
	 case 6:
	   builder.append ("VI");
	   break;
	 case 7:
	   builder.append ("VII");
	   break;
	 case 8:
	   builder.append ("VII");
	   break;
	 case 9:
	   builder.append ("IX");
	   break;
	 case 10:
	   builder.append ("X");
	   break;
	 default:
	   builder.append (level);
	   break;
    }
    return builder.toString ();
  }

  public ItemStack getEnchantmentBook(CustomEnchant enchant, int level){
    return new ItemStackBuilder (Material.ENCHANTED_BOOK).getDisplayName ("§d§kl§f" + enchant.name () + " enchantment" + levelFormat (level)).getNBTTag (enchant.name (), level).build ();
  }

  public ItemStack getEnchantmentBook(CustomEnchant enchant){
    return getEnchantmentBook (enchant, 1);
  }

  public int getEnchantLevel(ItemStack item, CustomEnchant enchant){
    return new NBTItem (item).getInteger (enchant.name ());
  }

  public ItemStack enchantItem(ItemStack item, CustomEnchant enchant, int level){
    return new ItemStackBuilder (item).getLore ("§7" + enchant.name () + levelFormat (level)).getNBTTag (enchant.name (), level).build ();
  }

  public ItemStack enchantItem(ItemStack item, CustomEnchant enchant){
    return enchantItem (item, enchant, 1);
  }

  public boolean isCompatible(ItemStack item, CustomEnchant.CustomEnchantType type){
    List<Material> allowedItems = new ArrayList<> ();
    switch (type){
	 case GLOBAL:
	   Collections.addAll (allowedItems, Material.values ());
	   break;
	 case ARMOR:
	   allowedItems.add (Material.LEATHER_BOOTS);
	   allowedItems.add (Material.LEATHER_LEGGINGS);
	   allowedItems.add (Material.LEATHER_CHESTPLATE);
	   allowedItems.add (Material.LEATHER_HELMET);
	   allowedItems.add (Material.CHAINMAIL_BOOTS);
	   allowedItems.add (Material.CHAINMAIL_LEGGINGS);
	   allowedItems.add (Material.CHAINMAIL_CHESTPLATE);
	   allowedItems.add (Material.CHAINMAIL_HELMET);
	   allowedItems.add (Material.GOLD_BOOTS);
	   allowedItems.add (Material.GOLD_LEGGINGS);
	   allowedItems.add (Material.GOLD_CHESTPLATE);
	   allowedItems.add (Material.GOLD_HELMET);
	   allowedItems.add (Material.IRON_BOOTS);
	   allowedItems.add (Material.IRON_LEGGINGS);
	   allowedItems.add (Material.IRON_CHESTPLATE);
	   allowedItems.add (Material.IRON_HELMET);
	   allowedItems.add (Material.DIAMOND_BOOTS);
	   allowedItems.add (Material.DIAMOND_LEGGINGS);
	   allowedItems.add (Material.DIAMOND_CHESTPLATE);
	   allowedItems.add (Material.DIAMOND_HELMET);
	   break;
	 case BOOTS:
	   allowedItems.add (Material.LEATHER_BOOTS);
	   allowedItems.add (Material.CHAINMAIL_BOOTS);
	   allowedItems.add (Material.GOLD_BOOTS);
	   allowedItems.add (Material.IRON_BOOTS);
	   allowedItems.add (Material.DIAMOND_BOOTS);
	   break;
	 case HELMET:
	   allowedItems.add (Material.LEATHER_HELMET);
	   allowedItems.add (Material.CHAINMAIL_HELMET);
	   allowedItems.add (Material.GOLD_HELMET);
	   allowedItems.add (Material.IRON_HELMET);
	   allowedItems.add (Material.DIAMOND_HELMET);
	   break;
	 case BOW:
	   allowedItems.add (Material.BOW);
	   break;
	 case TOOL:
	   allowedItems.add (Material.WOOD_PICKAXE);
	   allowedItems.add (Material.STONE_PICKAXE);
	   allowedItems.add (Material.GOLD_PICKAXE);
	   allowedItems.add (Material.IRON_PICKAXE);
	   allowedItems.add (Material.DIAMOND_PICKAXE);

	   allowedItems.add (Material.WOOD_AXE);
	   allowedItems.add (Material.STONE_AXE);
	   allowedItems.add (Material.GOLD_AXE);
	   allowedItems.add (Material.IRON_AXE);
	   allowedItems.add (Material.DIAMOND_AXE);

	   allowedItems.add (Material.WOOD_SPADE);
	   allowedItems.add (Material.STONE_SPADE);
	   allowedItems.add (Material.GOLD_SPADE);
	   allowedItems.add (Material.IRON_SPADE);
	   allowedItems.add (Material.DIAMOND_SPADE);
    }
    return allowedItems.contains (item.getType ());

  }

  public boolean hasEnchantment(ItemStack item, CustomEnchant enchant){
    try {
	 return new NBTItem (item).hasKey (enchant.name ());
    }catch (Exception x){
	 x.printStackTrace ();
	 return false;
    }
  }

  public List<CustomEnchant> getEnchantments(ItemStack item){
    List<CustomEnchant> customEnchants = new ArrayList<> ();
    for (CustomEnchant enchant : customEnchants){
	 if (new NBTItem (item).getInteger (enchant.name ())!=null) {
	   customEnchants.add (enchant);
	 }
    }
    return customEnchants;
  }
}
