package me.zombiegalaxydev.faction.enchants;
import me.zombiegalaxydev.faction.Core;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class CustomEnchantListener implements Listener{

  private CustomEnchantManager manager;

  public CustomEnchantListener () {
    this.manager = Core.get ().getCustomEnchantManager ();
    new BukkitRunnable () {
	 @Override
	 public void run () {
	   for (Player p : Bukkit.getOnlinePlayers ()){
		for (ItemStack item : p.getInventory ().getArmorContents ()){
		  if (item==null||item.getType ().equals (Material.AIR))return;
		  for (CustomEnchant enchant : CustomEnchant.values ()){
		    if (manager.hasEnchantment (item, enchant)){
			 switch (enchant){
			   case SHIELD:

			 }
		    }
		  }
		}
	   }
	 }
    }.runTaskTimer (Core.get (), 0L, 80L);
  }

  @EventHandler
  public void onDamage(EntityDamageByEntityEvent e){
    if (e.getDamager () instanceof Player){
	 Player p = (Player) e.getDamager ();
	 ItemStack item = p.getItemInHand ();
	 if (item==null||item.getType().equals(Material.AIR)||!(e.getDamager ()instanceof Player))return;
	 if (manager.hasEnchantment (item, CustomEnchant.THUNDER)){
	   if (new Random ().nextFloat () <= (0.10F + (manager.getEnchantLevel (item, CustomEnchant.THUNDER)*.05f))){
		for (int i = 0; i < manager.getEnchantLevel (item, CustomEnchant.THUNDER); i++) {
		  p.getWorld ().strikeLightning (e.getEntity ().getLocation ().add (0, 1, 0));
		}
	   }
	 }else if (manager.hasEnchantment (item, CustomEnchant.VENOM)){
	   if (new Random ().nextFloat () <= 0.15F&&e.getEntity ()instanceof Player){
		Player pl = (Player) e.getEntity ();
		pl.addPotionEffect (new PotionEffect (PotionEffectType.POISON, 20 * manager.getEnchantLevel (item, CustomEnchant.VENOM), 0));
		((Player) e.getDamager ()).playSound (e.getDamager ().getLocation (), Sound.CREEPER_HISS, 1, 1);
	   }
	 }else if (manager.hasEnchantment (item, CustomEnchant.LIFESTEAL)){
	   if (new Random ().nextFloat () <= 0.10F&&e.getEntity ()instanceof Player){
		int amount = 1 + manager.getEnchantLevel (item, CustomEnchant.LIFESTEAL);

		Location location = ((Player) e.getDamager ()).getEyeLocation ();
		PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles (EnumParticle.HEART, true, (float) location.getX (), (float) location.getY (), (float) location.getZ (), (float) .5, 0, (float) .5, 0, 5, null);
		((CraftPlayer) e.getDamager ()).getHandle ().playerConnection.sendPacket (packet);

		((Player) e.getDamager ()).setHealth (((Player) e.getDamager ()).getHealth () + amount);
		((Player) e.getEntity ()).setHealth (((Player) e.getEntity ()).getHealth () - amount);
	   }
	 }else if (manager.hasEnchantment (item, CustomEnchant.BLIND)){
	   if (new Random ().nextFloat () <= 0.20F&&e.getEntity ()instanceof Player){
		Player pl = (Player) e.getEntity ();
		pl.addPotionEffect (new PotionEffect (PotionEffectType.BLINDNESS, 100 * manager.getEnchantLevel (item, CustomEnchant.BLIND), 0));
		((Player) e.getDamager ()).playSound (e.getDamager ().getLocation (), Sound.BAT_IDLE, 1, 1);
	   }
	 }else if (manager.hasEnchantment (item, CustomEnchant.WITHER)){
	   if (new Random ().nextFloat () <= 0.15F&&e.getEntity ()instanceof Player){
		Player pl = (Player) e.getEntity ();
		pl.addPotionEffect (new PotionEffect (PotionEffectType.WITHER, 60 * manager.getEnchantLevel (item, CustomEnchant.WITHER), 0));
		((Player) e.getDamager ()).playSound (e.getDamager ().getLocation (), Sound.WITHER_IDLE, 1, 1);
	   }
	 }else if (manager.hasEnchantment (item, CustomEnchant.CRIPPLE)){
	   if (new Random ().nextFloat () <= 0.20F&&e.getEntity ()instanceof Player){
		Player pl = (Player) e.getEntity ();
		pl.addPotionEffect (new PotionEffect (PotionEffectType.CONFUSION, 100 * manager.getEnchantLevel (item, CustomEnchant.CRIPPLE), 0));
		pl.addPotionEffect (new PotionEffect (PotionEffectType.SLOW, 100 * manager.getEnchantLevel (item, CustomEnchant.CRIPPLE), 0));
		((Player) e.getDamager ()).playSound (e.getDamager ().getLocation (), Sound.CHICKEN_HURT, 1, 1);
	   }
	 }else if (manager.hasEnchantment (item, CustomEnchant.DEATHBRINGER)){
	   if (new Random ().nextFloat () <= 0.20F&&e.getEntity ()instanceof Player){
		Player pl = (Player) e.getEntity ();
		pl.damage (1 + (manager.getEnchantLevel (item, CustomEnchant.DEATHBRINGER)/e.getDamage ()));
		((Player) e.getDamager ()).playSound (e.getDamager ().getLocation (), Sound.IRONGOLEM_HIT, 1, 1);
	   }
	 }else if (manager.hasEnchantment (item, CustomEnchant.CHARGE)){
	   if (new Random ().nextFloat () <= 0.1F&&e.getEntity ()instanceof Player&&((Player) e.getDamager ()).isSprinting ()){
		e.setDamage (e.getDamage ()*(manager.getEnchantLevel (item, CustomEnchant.CHARGE)*.05));
		((Player) e.getDamager ()).playSound (e.getDamager ().getLocation (), Sound.WOLF_BARK, 1, 1);
	   }
	 }else if (manager.hasEnchantment (item, CustomEnchant.GORE)){
	   ((Player) e.getDamager ()).playSound (e.getDamager ().getLocation (), Sound.ENDERMAN_SCREAM, 1, 1);
	   if (new Random ().nextFloat () <= 0.1F&&e.getEntity ()instanceof Player){
		final Player pl = (Player) e.getEntity ();
		for (int i = 0; i < manager.getEnchantLevel (item, CustomEnchant.GORE); i++) {
		  new BukkitRunnable () {
		    @Override
		    public void run () {
			 pl.damage (2);
		    }
		  }.runTaskLater (Core.get (), 20 * i);
		}
	   }
	 }else if (manager.hasEnchantment (item, CustomEnchant.GOO)){
	   if (new Random ().nextFloat () <= 0.2F&&e.getEntity ()instanceof Player){
		Player pl = (Player) e.getEntity ();
		pl.addPotionEffect (new PotionEffect (PotionEffectType.JUMP, 40 * manager.getEnchantLevel (item, CustomEnchant.GOO), 4));
		((Player) e.getDamager ()).playSound (e.getDamager ().getLocation (), Sound.SLIME_WALK, 1, 1);
	   }
	 }else if (manager.hasEnchantment (item, CustomEnchant.FAMINE)){
	   if (new Random ().nextFloat () <= 0.1F){
		((Player) e.getDamager ()).setFoodLevel ((int) (((Player) e.getDamager ()).getFoodLevel () + e.getDamage ()));
		((Player) e.getDamager ()).playSound (((Player) e.getDamager ()).getEyeLocation (), Sound.EAT, 1, 1);
	   }
	 }
    }
  }

}
